//
//  RealmCodeGeneration.swift
//  Broadway
//
//  Created by Dmitry Cherednikov on 01/10/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmRepresentable
{
}

protocol RealmConvertible
{
}

protocol RealmFactoryRepresentable
{
}

class RealmUIColor: Object
{
    @objc dynamic var red   = Float()
    @objc dynamic var green = Float()
    @objc dynamic var blue  = Float()
    @objc dynamic var alpha = Float()
}

extension RealmModel
{
    static func toRealm(_ color: UIColor) -> RealmUIColor
    {
        let realmColor = RealmUIColor()

        let rgba = color.getRGBA()

        realmColor.red   = Float(rgba.red)
        realmColor.green = Float(rgba.green)
        realmColor.blue  = Float(rgba.blue)
        realmColor.alpha = Float(rgba.alpha)

        return realmColor
    }

    static func fromRealm(_ realmObject: RealmUIColor) -> UIColor
    {
        return UIColor(red: CGFloat(realmObject.red),
                       green: CGFloat(realmObject.green),
                       blue: CGFloat(realmObject.blue),
                       alpha: CGFloat(realmObject.alpha))
    }
}

extension UIColor: RealmConvertible
{
}

class RealmURL: Object
{
    @objc dynamic var string = String()
}

extension RealmModel
{
    static func toRealm(_ url: URL) -> RealmURL
    {
        let realmURL = RealmURL()

        realmURL.string = url.absoluteString

        return realmURL
    }

    static func fromRealm(_ realmObject: RealmURL) -> URL
    {
        return URL(string: realmObject.string)!
    }
}

extension URL: RealmConvertible
{
}
