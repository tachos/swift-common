//
//  NonEmptyStringValidator.swift
//  FoodPulse
//
//  Created by Vyacheslav Shakaev on 20.03.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

@available(*, deprecated)
func createNonEmptyStringValidator(withError message: String = "Expected a non-empty string") -> ValidateCallback
{
    return {
        text in
        
        return !text.isEmpty ? .ok : .notOk(message)
    }
}

func createNonEmptyStringValidator(
    withError geLocalizedMessage: @escaping (() -> String)
    ) -> ValidateCallback
{
    return {
        text in
        
        return !text.isEmpty ? .ok : .notOk(geLocalizedMessage())
    }
}
