//
//  ValidationResult.swift
//  Sellel
//
//  Created by Ivan Goremykin on 16.01.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

typealias ValidateCallback = (String) -> ValidationResult

enum ValidationResult
{
    case ok
    case notOk(String)
}

extension ValidationResult
{
    var validationMessage: String?
    {
        switch self
        {
        case .ok:
            return nil
            
        case .notOk(let validationMessage):
            return validationMessage
        }
    }
}
