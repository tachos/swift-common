//
//  LengthValidator.swift
//  FoodPulse
//
//  Created by Vyacheslav Shakaev on 20.03.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

@available(*, deprecated)
func createLengthValidator(minLength: Int,
                           maxLength: Int,
                           minLengthErrorMessage: String? = nil,
                           maxLengthErrorMessage: String? = nil) -> ValidateCallback
{
    return {
        text in

        let defaultErrorMessage = "Expected \(minLength)-\(maxLength) characters"

        if text.count < minLength
        {
            return .notOk(localizeValidator(minLengthErrorMessage ?? defaultErrorMessage))
        }
        else if text.count > maxLength
        {
            return .notOk(localizeValidator(maxLengthErrorMessage ?? defaultErrorMessage))
        }
        else
        {
            return .ok
        }
    }
}

func createLengthValidator(
    minLength: Int,
    maxLength: Int,
    minLengthErrorMessage: @escaping (() -> String),
    maxLengthErrorMessage: @escaping (() -> String)
    ) -> ValidateCallback
{
    return {
        text in
        
        if text.count < minLength
        {
            return .notOk(minLengthErrorMessage())
        }
        else if text.count > maxLength
        {
            return .notOk(maxLengthErrorMessage())
        }
        else
        {
            return .ok
        }
    }
}
