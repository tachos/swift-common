//
//  MinLengthValidation.swift
//  Sellel
//
//  Created by Ivan Goremykin on 16.01.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

@available(*, deprecated)
func createMinLengthValidator(minLength: Int, withError message: String) -> ValidateCallback
{
    return {
        text in
        
        return text.count >= minLength ? .ok : .notOk(message)
    }
}

func createMinLengthValidator(
    minLength: Int,
    withError geLocalizedMessage: @escaping (() -> String)
    ) -> ValidateCallback
{
    return {
        text in
        
        return text.count >= minLength
            ? .ok
            : .notOk(geLocalizedMessage())
    }
}
