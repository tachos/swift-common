//
//  AlphanumericValidator.swift
//  Broadway
//
//  Created by Michael Goremykin on 14/06/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

func createAlphanumericValidator(withError message: String = "Expected only letters and digits") -> ValidateCallback
{
    return {
        text in
        
        return CharacterSet.alphanumerics.isSuperset(of: CharacterSet(charactersIn: text)) ?
            .ok : .notOk(message)
    }
}
