//
//  NecessaryCharacterSetsValidator.swift
//  Broadway
//
//  Created by Dmitry Cherednikov on 11/07/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

func createNecessaryCharacterSetsValidator(_ characterSetsErrorPairs: (CharacterSet, String)...) -> ValidateCallback
{
    return {
        text in

        let charactersInText = CharacterSet(charactersIn: text)

        for characterSetsErrorPair in characterSetsErrorPairs
        {
            if characterSetsErrorPair.0.intersection(charactersInText).isEmpty
            {
                return .notOk(localizeValidator(characterSetsErrorPair.1))
            }
        }

        return .ok
    }
}
