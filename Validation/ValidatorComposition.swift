//
//  ValidatorComposition.swift
//  FoodPulse
//
//  Created by Vyacheslav Shakaev on 19.03.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import Foundation

func composeValidators(_ callbacks: ValidateCallback...)  -> ValidateCallback
{
    return {
        text in
        
        for callback in callbacks
        {
            if case let .notOk(errorMessage) = callback(text)
            {
                return .notOk(errorMessage)
            }
        }

        return .ok
    }
}

