//
//  AlphaNumericStringValidator.swift
//  Broadway
//
//  Created by Dmitry Cherednikov on 17/07/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

@available(*, deprecated)
func createAlphaNumericStringValidator(withError message: String = "Only alpha numerics characters are allowed") -> ValidateCallback
{
    return {
        text in

        return CharacterSet.alphanumerics.isSuperset(of: CharacterSet(charactersIn: text)) ?
            .ok : .notOk(message)
    }
}

@available(*, deprecated)
func createAlphaNumericSpacedStringValidator(withError message: String = "Only alpha numerics characters are allowed") -> ValidateCallback
{
    return {
        text in

        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(" ")
        
        return characterSet.isSuperset(of: CharacterSet(charactersIn: text)) ?
            .ok : .notOk(message)
    }
}

func createAlphaNumericStringValidator(
    withError geLocalizedMessage: @escaping (() -> String)
    ) -> ValidateCallback
{
    return {
        text in
        
        return CharacterSet.alphanumerics.isSuperset(of: CharacterSet(charactersIn: text))
            ? .ok
            : .notOk(geLocalizedMessage())
    }
}

func createAlphaNumericSpacedStringValidator(
    withError geLocalizedMessage: @escaping (() -> String)
    ) -> ValidateCallback
{
    return {
        text in
        
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(" ")
        
        return characterSet.isSuperset(of: CharacterSet(charactersIn: text))
            ? .ok
            : .notOk(geLocalizedMessage())
    }
}
