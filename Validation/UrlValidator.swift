//
//  UrlValidator.swift
//  FoodPulse
//
//  Created by Vyacheslav Shakaev on 19.03.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//
import UIKit

func createUrlValidator(errorMessage: String? = nil) -> ValidateCallback
{
    return {
        urlString in
        
        // http://urlregex.com
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[urlRegEx])
        return predicate.evaluate(with: urlString) ? .ok : .notOk(errorMessage ?? "Invalid URL")
    }
}
