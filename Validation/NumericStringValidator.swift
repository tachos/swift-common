//
//  NumericStringValidator.swift
//  FoodPulse
//
//  Created by Vyacheslav Shakaev on 20.03.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//
import Foundation

func createNumericStringValidator(withError message: String = "Expected only digits") -> ValidateCallback
{
    return {
        text in
        
        return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: text)) ?
            .ok : .notOk(message)
    }
}
