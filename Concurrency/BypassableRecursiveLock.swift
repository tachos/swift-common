//  Created by Ivan Goremykin on 15/03/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

struct BypassableRecursiveLock
{
    private let recursiveLock: NSRecursiveLock?
    
    init(bypass: Bool)
    {
        recursiveLock = bypass ? nil : NSRecursiveLock()
    }
    
    func execute(_ callback: VoidCallback)
    {
        recursiveLock?.lock()
        
        callback()
        
        recursiveLock?.unlock()
    }
}
