//  Created by Ivan Goremykin on 15/03/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

struct RecursiveLock
{
    private let lock = NSRecursiveLock()
    
    func execute(_ callback: VoidCallback)
    {
        lock.lock()
        
        callback()
        
        lock.unlock()
    }
}
