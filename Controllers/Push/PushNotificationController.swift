//
//  PushNotificationController.swift
//  FoodPulse
//
//  Created by Vyacheslav Shakaev on 17.04.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//
import RxSwift
import UIKit
import UserNotifications

typealias DeviceNotificationToken = String

typealias PushNotificationParameters = [AnyHashable: Any]

/// Represents application-specific logic.
/// Responsible for updating the device token on the remote server
/// and executing push notifications.
protocol PushNotificationClient
{
    /// Compare up-to-date device token with stored onedevice
    /// @param latestDeviceToken is nil when running in Simulator
    func updateDeviceTokenIfNeeded(latestDeviceToken: String?)

    ///
    func executePushOnLaunch(_ dictionary: PushNotificationParameters)
    
    ///
    func executePushOnTap(_ dictionary: PushNotificationParameters)
    
    ///
    func executePushForeground(_ dictionary: PushNotificationParameters)
}

class PushNotificationController: NSObject
{
    static let instance = PushNotificationController()
    
    let disposeBag = DisposeBag()

    var notificationTappedBeforeAppLaunch: PushNotificationParameters?

    public private(set) var latestDeviceToken: DeviceNotificationToken?
    
    private var client: PushNotificationClient!
}

extension PushNotificationController
{
    /// Should be called in AppDelegate: application:didFinishLaunchingWithOptions, e.g.:
    /// PushNotificationController.instance.setup(with: launchOptions)
    func setup(client: PushNotificationClient, with launchOptions: [UIApplication.LaunchOptionsKey : Any]?)
    {
        self.client = client

        // make sure the user is asked to register for push notifications at app start-up
        //registerForPushNotifications()
        
        // For iOS < 10
        getPushNotification(from: launchOptions)

        if let notificationPayload = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? PushNotificationParameters
        {
            // NOTE: cannot executePushOnLaunch(pushNotification) before the UI is setup
            notificationTappedBeforeAppLaunch = notificationPayload

            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
    func registerForPushNotifications()
    {
        if #available(iOS 10.0 , *)
        {
            let center = UNUserNotificationCenter.current()
            
            center.requestAuthorization(options: [.badge, .alert, .sound])
            {
                (granted, error) in
                
                debugPrint("Push Notifications: Permission granted?: \(granted)")
                
                // Attempt registration for remote notifications on the main thread
                DispatchQueue.main.async
                {
                    UIApplication.shared.registerForRemoteNotifications()
                    center.delegate = self
                }
            }
        }
        else
        {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    /// should be called from UIApplication::didRegisterForRemoteNotificationsWithDeviceToken()
    func onRegisteredForRemoteNotifications(deviceToken: Data)
    {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        /// Sets a device token to register the current application installation for push notifications.
        print(deviceTokenString.lowercased())
        
        /// get deviceToken by Ok button from special alert
//        topmostViewController.alert(title: "Device token received", message: "Tap OK to copy to clipboard", buttonCallback:
//            {
//                UIPasteboard.general.string = deviceTokenString.lowercased()
//            })

        latestDeviceToken = deviceTokenString
        
        client.updateDeviceTokenIfNeeded(latestDeviceToken: latestDeviceToken)
    }

    /// Should be called from UIApplication::didFailToRegisterForRemoteNotificationsWithError()
    func onFailedToRegisterForRemoteNotifications(error: Error)
    {
        print("❤️Remote notification support is unavailable due to error:\n\(error)")
    }

    /// Should be called after showing the Main screen.
    func executeNotificationTappedBeforeAppLaunch()
    {
        if let pendingPushNotification = notificationTappedBeforeAppLaunch
        {
            client.executePushOnLaunch(pendingPushNotification)
            notificationTappedBeforeAppLaunch = nil
        }
    }
    
    func updateDeviceTokenIfNeeded()
    {
        client.updateDeviceTokenIfNeeded(latestDeviceToken: latestDeviceToken)
    }
}

// MARK:- Notifications iOS >= 10
extension PushNotificationController: UNUserNotificationCenterDelegate
{
    // App is in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void)
    {
        let userInfo = notification.request.content.userInfo

        client.executePushForeground(userInfo)

        completionHandler(.alert)
    }

    // App is inactive or is in background
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void)
    {
        let userInfo = response.notification.request.content.userInfo

        client.executePushOnTap(userInfo)
        
        completionHandler()
    }
}

// MARK:- Notifications iOS < 10
extension PushNotificationController
{
    func getPushNotification(from launchOptions: [UIApplication.LaunchOptionsKey : Any]?)
    {
        if #available(iOS 10.0, *)
        {
        }
        else
        {
            if let launchOptions = launchOptions
            {
                if let userInfo = launchOptions[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable : Any]
                {
                    client.executePushOnLaunch(userInfo)
                }
            }
        }
    }
}
