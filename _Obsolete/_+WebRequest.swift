//
//  UIViewController+WebService.swift
//  Sellel
//
//  Created by Ivan Goremykin on 31.08.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import Alamofire
import UIKit

// MARK:- Input (Client -> Server)
extension UIViewController
{
    func makeInRequestWithFreezeAndAlert<RequestType>(request: @escaping (RequestType, @escaping (ResponseCode) -> Void) -> AsyncHandle,
                                                       requestObject: RequestType,
                                                       failureMessage: String,
                                                       onFailure handleFailure: ((RequestType, ResponseCode) -> Void)? = nil,
                                                       onSuccess handleSuccess: @escaping (RequestType) -> Void)
    {
        makeInRequest_Obsolete(request:       request,
                     requestObject: requestObject,
                     onStart:       freezeStart,
                     onCompletion:  freezeCompletion,
                     onFailure:     createInOutAlertFailureCallback(failureMessage, handleFailure),
                     onSuccess:     handleSuccess)
    }
}

// MARK:- Input / Output
extension UIViewController
{
    func makeInOutRequestWithFreezeAndAlert<RequestType, ResponseType>(request: @escaping (RequestType, @escaping (ResponseCode, ResponseType?) -> Void) -> AsyncHandle,
                                            requestObject: RequestType,
                                            failureMessage: String,
                                            onCancel  handleCancel:  VoidCallback? = nil,
                                            onFailure handleFailure: ((RequestType, ResponseCode) -> Void)? = nil,
                                            onSuccess handleSuccess: @escaping (RequestType, ResponseType) -> Void)
    {
        _ = makeInOutRequest_Obsolete(request:       request,
                             requestObject: requestObject,
                             onStart:       freezeStart,
                             onCompletion:  freezeCompletion,
                             onCancel:      handleCancel,
                             onFailure:     createInOutAlertFailureCallback(failureMessage, handleFailure),
                             onSuccess:     handleSuccess)
    }
    
    func makeInOutRequestWithFreezeAndOptionalAlert<RequestType, ResponseType>(request: @escaping (RequestType, @escaping (ResponseCode, ResponseType?) -> Void) -> AsyncHandle,
                                                    requestObject: RequestType,
                                                    failureMessage: String,
                                                    failureCallback: ((RequestType, ResponseCode) -> Bool)? = nil,
                                                    successCallback: @escaping (RequestType, ResponseType) -> Void)
    {
        _ = makeInOutRequest_Obsolete(request:       request,
                             requestObject: requestObject,
                             onStart:       freezeStart,
                             onCompletion:  freezeCompletion,
                             onFailure:     createInOutOptionalAlertFailureCallback(failureMessage, failureCallback),
                             onSuccess:     successCallback)
    }
    
    func makeUploadRequestWithFreezeAndAlert<RequestType, ResponseType>(request: @escaping (RequestType, @escaping (ResponseCode, ResponseType?) -> Void) -> Void,
                                                                       requestObject: RequestType,
                                                                       failureMessage: String,
                                                                       onCancel  handleCancel:  VoidCallback? = nil,
                                                                       onFailure handleFailure: ((RequestType, ResponseCode) -> Void)? = nil,
                                                                       onSuccess handleSuccess: @escaping (RequestType, ResponseType) -> Void)
    {
        makeUploadRequest_Obsolete(request:       request,
                          requestObject: requestObject,
                          onStart:       freezeStart,
                          onCompletion:  freezeCompletion,
                          onCancel:      handleCancel,
                          onFailure:     createInOutAlertFailureCallback(failureMessage, handleFailure),
                          onSuccess:     handleSuccess)
    }
}

// MARK:- Output
extension UIViewController
{
    func makeOutRequestWithFreezeAndAlert<ResponseType>(request: @escaping (@escaping (ResponseCode, ResponseType?) -> Void) -> AsyncHandle,
                                          failureMessage: String,
                                          onFailure handleFailure: ((ResponseCode) -> Void)? = nil,
                                          onSuccess handleSuccess: @escaping (ResponseType) -> Void)
    {
        makeOutRequest_Obsolete(request:      request,
                       onStart:      freezeStart,
                       onCompletion: freezeCompletion,
                       onFailure:    createOutAlertFailureCallback(failureMessage, handleFailure),
                       onSuccess:    handleSuccess)
    }
    
    func makeOutRequestWithFreezeAndOptionalAlert<ResponseType>(request: @escaping (@escaping (ResponseCode, ResponseType?) -> Void) -> AsyncHandle,
                                                  failureMessage: String,
                                                  failureCallback: ((ResponseCode) -> Bool)? = nil,
                                                  successCallback: @escaping (ResponseType) -> Void)
    {
        makeOutRequest_Obsolete(request:      request,
                       onStart:      freezeStart,
                       onCompletion: freezeCompletion,
                       onFailure:    createOutOptionalAlertFailureCallback(failureMessage, failureCallback),
                       onSuccess:    successCallback)
    }
}

// MARK:- Freeze callbacks
extension UIViewController
{
    fileprivate func freezeStart()
    {
        freezeUI()
    }
    
    fileprivate func freezeCompletion()
    {
        unfreezeUI()
    }
}

// MARK:- Alert / Failure callback
private extension UIViewController
{
    // MARK:- In / Out
    func createInOutOptionalAlertFailureCallback<RequestType>(_ message: String, _ failureCallback: ((RequestType, ResponseCode) -> Bool)? = nil) -> (RequestType, ResponseCode) -> Void
    {
        return { requestObject, responseCode in
            
            if failureCallback == nil || failureCallback!(requestObject, responseCode)
            {
                self.alert(title: "Error", message: message)
            }
        }
    }
    
    func createInOutAlertFailureCallback<RequestType>(_ message: String, _ failureCallback: ((RequestType, ResponseCode) -> Void)? = nil) -> (RequestType, ResponseCode) -> Void
    {
        return { requestObject, responseCode in
            
            failureCallback?(requestObject, responseCode)
            self.alert(title: "Error", message: message)
        }
    }
    // MARK:- Out
    func createOutOptionalAlertFailureCallback(_ message: String, _ failureCallback: ((ResponseCode) -> Bool)? = nil) -> (ResponseCode) -> Void
    {
        return { responseCode in
            
            if failureCallback == nil || failureCallback!(responseCode)
            {
                self.alert(title: "Error", message: message)
            }
        }
    }
    
    func createOutAlertFailureCallback(_ message: String, _ failureCallback: ((ResponseCode) -> Void)? = nil) -> (ResponseCode) -> Void
    {
        return { responseCode in
            
            failureCallback?(responseCode)
            self.alert(title: "Error", message: message)
        }
    }
}
