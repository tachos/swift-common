//
//  MakeTransaction.swift
//  Sellel
//
//  Created by Ivan Goremykin on 08.11.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//
import Alamofire

// MARK:- In
// MARK: Symmetric
func makeInTransaction<RequestType, TransactionType>(requestObject:                   RequestType,
                                                     request:                         @escaping (RequestType, @escaping (ResponseCode) -> Void) -> AsyncHandle,
                                                     previousValue:                   TransactionType,
                                                     newValue:                        TransactionType,
                                                     onApplyValue applyValueCallback: @escaping (TransactionType) -> Void,
                                                     onFailure failureCallback:       ((RequestType, ResponseCode) -> Void)? = nil,
                                                     onSuccess successCallback:       @escaping (RequestType) -> Void)
{
    makeInRequest_Obsolete(request:         request,
                  requestObject:   requestObject,
                  onStart:   bind(applyValueCallback, newValue),
                  onFailure: sequence2(suppress2(bind(applyValueCallback, previousValue)), failureCallback),
                  onSuccess: successCallback)
}

// MARK: Asymmetric
func makeInTransaction<RequestType>(requestObject:             RequestType,
                                    request:                   @escaping (RequestType, @escaping (ResponseCode) -> Void) -> AsyncHandle,
                                    onApply applyCallback:     @escaping VoidCallback,
                                    onRevert revertCallback:   @escaping VoidCallback,
                                    onFailure failureCallback: ((RequestType, ResponseCode) -> Void)? = nil,
                                    onSuccess successCallback: @escaping (RequestType) -> Void)
{
    makeInRequest_Obsolete(request:         request,
                  requestObject:   requestObject,
                  onStart:   applyCallback,
                  onFailure: sequence2(suppress2(revertCallback), failureCallback),
                  onSuccess: successCallback)
}
