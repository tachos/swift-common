//  Created by Ivan Goremykin on 19/11/2018.
//  Copyright © 2018 Averia. All rights reserved.

// MARK:- Out
func map<ResponseValue, MappedResponseValue>(_ outRequest: @escaping OutRequest<ResponseValue>,
                                             _ transform: @escaping (ResponseValue) -> MappedResponseValue)
                                                    -> OutRequest<MappedResponseValue>
{
    return {
        mappedAsyncResultHandler in
        
        return outRequest {
            asyncResult in
            
            switch asyncResult
            {
            case .success(let responseValue):
                mappedAsyncResultHandler(.success(transform(responseValue)))
                
            case .failure(let error):
                mappedAsyncResultHandler(.failure(error))
                
            case .cancel:
                mappedAsyncResultHandler(.cancel)
            }
        }
    }
}

// MARK:- In
func map<RequestParameter, MappedRequestParameter>(_ inRequest: @escaping InRequest<RequestParameter>,
                                                   _ reverseTransform: @escaping (MappedRequestParameter) -> RequestParameter)
    -> InRequest<MappedRequestParameter>
{
    return chainLeft(reverseTransform, inRequest)
}
