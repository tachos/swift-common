//
//  Request.swift
//  BBC
//
//  Created by Ivan Goremykin on 16.08.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import Foundation

typealias InOutRequest<RequestParameter, ResponseValue> = (RequestParameter, @escaping AsyncResultHandler<ResponseValue>) -> AsyncHandle
typealias OutRequest<ResponseValue> = (@escaping AsyncResultHandler<ResponseValue>) -> AsyncHandle
typealias InRequest<RequestParameter> = (RequestParameter, @escaping AsyncResultHandler<Void>) -> AsyncHandle

typealias InOutPaginatedRequest<RequestParameter, Item> = (RequestParameter, Page, @escaping AsyncResultHandler<[Item]>) -> AsyncHandle
typealias OutPaginatedRequest<Item> = (Page, @escaping AsyncResultHandler<[Item]>) -> AsyncHandle

//------------------------------------------------------------------------------------------------//

typealias RequestFailureHandler<RequestParameter> = (RequestParameter, Error) -> Void

typealias InOutRequestSuccessHandler<RequestParameter, ResponseValue> = (RequestParameter, ResponseValue) -> Void

typealias OutRequestSuccessHandler<ResponseValue> = (ResponseValue) -> Void

//------------------------------------------------------------------------------------------------//

// MARK:- InOut
func performInOutRequest<RequestParameter, ResponseValue>(
        request: @escaping InOutRequest<RequestParameter, ResponseValue>,
        requestParameter: RequestParameter,
        onStart      handleStart:      VoidCallback? = nil,
        onCompletion handleCompletion: VoidCallback? = nil,
        onCancel     handleCancel:     VoidCallback? = nil,
        onFailure    handleFailure:    RequestFailureHandler<RequestParameter>? = nil,
        onSuccess    handleSuccess:    @escaping InOutRequestSuccessHandler<RequestParameter, ResponseValue>)
            -> AsyncHandle
{
    handleStart?()
    
    return request(requestParameter)
    {
        asyncResult in
        
        switch asyncResult
        {
        case .cancel:
            handleCancel?()
            
        case .success(let value):
            handleSuccess(requestParameter, value)
            
        case .failure(let error):
            handleFailure?(requestParameter, error)
        }
        
        handleCompletion?()
    }
}

// MARK:- Out
@discardableResult
func performOutRequest<ResponseValue>(
        request: @escaping OutRequest<ResponseValue>,
        onStart      handleStart:      VoidCallback? = nil,
        onCompletion handleCompletion: VoidCallback? = nil,
        onCancel     handleCancel:     VoidCallback? = nil,
        onFailure    handleFailure:    ErrorHandler? = nil,
        onSuccess    handleSuccess:    @escaping OutRequestSuccessHandler<ResponseValue>)
            -> AsyncHandle
{
    handleStart?()
    
    return request
        {
            asyncResult in
            
            switch asyncResult
            {
            case .cancel:
                handleCancel?()
                
            case .success(let value):
                handleSuccess(value)
                
            case .failure(let error):
                handleFailure?(error)
            }
            
            handleCompletion?()
    }
}

// MARK:- In
func performInRequest<RequestParameter>(
        request: @escaping InRequest<RequestParameter>,
        requestObject: RequestParameter,
        onStart      handleStart:      VoidCallback? = nil,
        onCompletion handleCompletion: VoidCallback? = nil,
        onCancel     handleCancel:     VoidCallback? = nil,
        onFailure    handleFailure:    RequestFailureHandler<RequestParameter>? = nil,
        onSuccess    handleSuccess:    @escaping (RequestParameter) -> Void)
            -> AsyncHandle
{
    handleStart?()
    
    return request(requestObject)
    {
        asyncResult in
        
        switch asyncResult
        {
        case .cancel:
            handleCancel?()
            
        case .success:
            handleSuccess(requestObject)
            
        case .failure(let error):
            handleFailure?(requestObject, error)
        }
        
        handleCompletion?()
    }
}
