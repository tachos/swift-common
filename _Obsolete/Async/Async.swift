//  Created by Ivan Goremykin on 16.08.2018.
//  Copyright © 2018 Tachos. All rights reserved.

import Foundation

enum AsyncResult<Type>
{
    case success(Type)
    case failure(Error)
    case cancel
}

extension AsyncResult
{
    var isSuccess: Bool
    {
        switch self
        {
        case .success:
            return true
            
        case .cancel, .failure:
            return false
        }
    }
}

protocol AsyncHandle
{
    func cancel()
}

typealias AsyncResultHandler<Type> = (AsyncResult<Type>) -> Void
typealias AsyncHandler = (AsyncResult<Void>) -> Void
