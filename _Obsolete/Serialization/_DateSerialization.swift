//
//  DateSerialization.swift
//  FoodPulse
//
//  Created by Ivan Goremykin on 06.03.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import Foundation

func serializeToString(_ date: Date) -> String
{
    return createDateFormatter().string(from: date)
}

func deserializeToDate(_ string: String) -> Date?
{
    return createDateFormatter().date(from: string)
}

private func createDateFormatter() -> DateFormatter
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    
    return dateFormatter
}
