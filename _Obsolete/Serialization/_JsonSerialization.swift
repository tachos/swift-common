//
//  JsonSerialization.swift
//  FoodPulse
//
//  Created by Vyacheslav Shakaev on 18.04.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import SwiftyJSON

func serializeToJsonString(_ dictionary: [String : Any]) -> String
{
    let jsonData = try! JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions()) as Data
    let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
    
    return jsonString
}

func serializeToJsonString<Type>(_ array: [Type], _ serialize: (Type) -> String) -> String
{
    return "[" + array.map(serialize).joined(separator: ",") + "]"
}
