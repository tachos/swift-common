//
//  JsonParsing.swift
//  StarHub
//
//  Created by Ivan Goremykin on 02.06.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import SwiftyJSON

func parseString(_ json: JSON, _ nodeName: String) -> String?
{
    return json[nodeName].string
}

func parseStringOrEmpty(_ json: JSON, _ nodeName: String) -> String
{
    return parseString(json, nodeName) ?? ""
}

func parseBool(_ json: JSON, _ nodeName: String) -> Bool?
{
    return json[nodeName].bool
}

func parseInt(_ json: JSON, _ nodeName: String) -> Int?
{
    return json[nodeName].int
}

func parseFloat(_ json: JSON, _ nodeName: String) -> Float?
{
    return json[nodeName].float
}

func parseDouble(_ json: JSON, _ nodeName: String) -> Double?
{
    return json[nodeName].double
}

func parseUrl(_ json: JSON, _ nodeName: String) -> URL?
{
    // Parse
    let stringUrl = parseString(json, nodeName)
    
    // Validate
    if anyIsNil(stringUrl)
    {
        return nil
    }
    
    // Construct
    return URL(string: stringUrl!)
}

func parseStringDictionary(_ json: JSON, _ nodeName: String) -> [String : String]?
{
    // Parse
    let dictionary = json[nodeName].dictionary
    
    // Validate
    if anyIsNil(dictionary)
    {
        return nil
    }
    
    // Construct
    var stringDictionary: [String : String] = [:]
    for (key, value) in dictionary!
    {
        stringDictionary[key] = value.stringValue
    }
    
    return stringDictionary
}

func parseUnixDate(_ json: JSON, _ nodeName: String) -> Date?
{
    // Parse.
    let timeString = json[nodeName].string
    
    // Validate.
    if anyIsNil(timeString)
    {
        return nil
    }
    
    // Convert to Double.
    if let timeMilliseconds = Double(timeString!)
    {
        return Date(timeIntervalSince1970: timeMilliseconds / 1000)
    }
    
    return nil
}

func parseFormattedDate(_ json: JSON, _ nodeName: String, dateFormat: String) -> Date?
{
    // Parse.
    let timeString = json[nodeName].string
    
    // Validate.
    if anyIsNil(timeString)
    {
        return nil
    }
    
    let formatter = DateFormatter()
    formatter.dateFormat = dateFormat
    let date = formatter.date(from: timeString!)
    
    return date
}

func parseIso8601Date(_ json: JSON, _ nodeName: String) -> Date?
{
    return parseFormattedDate(json, nodeName, dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSxxx")
}

func parseIso8601DateWithoutMs(_ json: JSON, _ nodeName: String) -> Date?
{
    return parseFormattedDate(json, nodeName, dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
}

func parseBigEndianDate(_ json: JSON, _ nodeName: String) -> Date?
{
    return parseFormattedDate(json, nodeName, dateFormat: "yyyy-MM-dd")
}

func parseArray<Type>(_ json: JSON, _ parseJson: (JSON) -> Type?) -> [Type]?
{
    // Parse.
    let jsonArray = json.array
    
    // Validate.
    if jsonArray == nil
    {
        return nil
    }
    
    var array: [Type] = []
    for json in jsonArray!
    {
        let object = parseJson(json)
        
        if object == nil
        {
            return nil
        }
        
        array.append(object!)
    }
    
    return array
}

func parseSet<Type>(_ json: JSON, _ parseJson: (JSON) -> Type?) -> Set<Type>?
{
    if let array = parseArray(json, parseJson)
    {
        return Set(array)
    }
    
    return nil
}

func parseSetFilterNil<Type>(_ json: JSON, _ parseJson: (JSON) -> Type?) -> Set<Type>?
{
    if let array = parseArrayFilterNil(json, parseJson)
    {
        return Set(array)
    }

    return nil 
}

func parseArrayFilterNil<Type>(_ json: JSON, _ parseJson: (JSON) -> Type?) -> [Type]?
{
    // Parse.
    let jsonArray = json.array
    
    // Validate.
    if jsonArray == nil
    {
        return nil
    }
    
    var array: [Type] = []
    for json in jsonArray!
    {
        if let object = parseJson(json)
        {
            array.append(object)
        }
    }
    
    return array
}

func parseStringArray(_ json: JSON) -> [String]?
{
    return parseArray(json, { $0.string })
}

func parseUrlArray(_ json: JSON) -> [URL]
{
    if let array = json.array
    {
        return array.compactMap{ URL(string: $0.stringValue) }
    }
    
    return []
}

func parseIntArray(_ json: JSON, _ nodeName: String) -> [Int]?
{
    return parseArray(json[nodeName], { $0.int })
}
