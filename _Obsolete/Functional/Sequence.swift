//
//  Sequence.swift
//  Sellel
//
//  Created by Ivan Goremykin on 31.08.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

func sequence(_ callbacks: OptionalVoidCallback...) -> VoidCallback
{
    return { callbacks.forEach{ $0?() } }
}

func sequence1<A>(_ callbacks: ((A) -> Void)?...) ->
    (A) -> Void
{
    return {
        a in
        
        callbacks.forEach{ $0?(a) }
    }
}

func sequence2<A, B>(_ callbacks: ((A, B) -> Void)?...) ->
    (A, B) -> Void
{
    return {
        a, b in
        
        callbacks.forEach{ $0?(a, b) }
    }
}
