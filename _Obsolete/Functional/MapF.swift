//  Created by Ivan Goremykin on 27/03/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

func mapF<A, B, R>(_ f: @escaping (A, A) -> R, _ transform: @escaping (B) -> A) -> (B, B) -> R
{
    return {
        b1, b2 in
        
        return f(transform(b1), transform(b2))
    }
}
