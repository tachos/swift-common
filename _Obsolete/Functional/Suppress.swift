//
//  SuppressParameters.swift
//  Sellel
//
//  Created by Ivan Goremykin on 06.09.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

// TODO: ignore

func suppress<A, B>(_ f: @escaping () -> B) -> (A) -> B
{
    return { _ in return f() }
}

func suppressOptional<A, B>(_ f: (() -> B)?) -> ((A) -> B)?
{
    if let f = f
    {
        return { _ in f() }
    }
    else
    {
        return nil
    }
}

func suppress2<A, B, C>(_ f: @escaping () -> C) -> (A, B) -> C
{
    return { _, _ in f() }
}

func suppressLeft<A, B, R>(_ f: @escaping (B) -> R) -> (A, B) -> R
{
    return { _, b in return f(b) }
}

func suppressLeft<A, B, C, R>(_ f: @escaping (B, C) -> R) -> (A, B, C) -> R
{
    return { _, b, c in return f(b, c) }
}

func suppressRight<A, B>(_ f: @escaping (A) -> Void) -> (A, B) -> Void
{
    return { a, _ in f(a) }
}

func suppressRight<A, B, R>(_ f: @escaping (A) -> R) -> (A, B) -> R
{
    return { a, _ in return f(a) }
}

func suppressLeftOptional<A, B>(_ f: ((B) -> Void)?) -> ((A, B) -> Void)?
{
    if let f = f
    {
        return { _, b in f(b) }
    }
    else
    {
        return nil
    }
}

func suppressRightOptional<A, B>(_ f: ((A) -> Void)?) -> ((A, B) -> Void)?
{
    if let f = f
    {
        return { a, _ in f(a) }
    }
    else
    {
        return nil
    }
}
