//  Created by Ivan Goremykin on 27/03/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

func discard<R, A>(_ f: @escaping (A) -> R) -> (A) -> Void
{
    return {
        a in
        
        _ = f(a)
    }
}
