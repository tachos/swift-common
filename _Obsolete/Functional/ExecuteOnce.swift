//
//  ExecuteOnce.swift
//  Broadway
//
//  Created by Ivan Goremykin on 17/04/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

func executeOnce(_ callback: @escaping VoidCallback) -> VoidCallback
{
    var _callback: VoidCallback? = callback
    
    return {
        _callback?()
        _callback = nil
    }
}
