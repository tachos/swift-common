//  Created by Ivan Goremykin on 05.09.17..

func bind<A, R>(_ f: @escaping (A) -> R, _ a: A) -> () -> R
{
    return { return f(a) }
}

func bind<A, B, R>(_ f: @escaping (A, B) -> R, _ a: A, _ b: B) -> () -> R
{
    return { return f(a, b) }
}

func bind<A, B, C, R>(_ f: @escaping (A, B, C) -> R, _ a: A, _ b: B, _ c: C) -> () -> R
{
    return { return f(a, b, c) }
}

func bind<A, B, C, D, R>(_ f: @escaping (A, B, C, D) -> R, _ a: A, _ b: B, _ c: C, _ d: D) -> () -> R
{
    return { return f(a, b, c, d) }
}

func bindRight<A, B, C>(_ f: @escaping (A, B) -> C, _ b: B) -> (A) -> C
{
    return { a in return f(a, b) }
}

func bindLeft<A, B, C>(_ f: @escaping (A, B) -> C, _ a: A) -> (B) -> C
{
    return { b in return f(a, b) }
}

func bindLeft<A, B, C, R>(_ f: @escaping (A, B, C) -> R, _ a: A, _ b: B) -> (C) -> R
{
    return { c in return f(a, b, c) }
}

func bindLeft<A, B, C, R>(_ f: @escaping (A, B, C) -> R, _ a: A) -> (B, C) -> R
{
    return { b, c in return f(a, b, c) }
}

func bindLeft<A, B, C, D, R>(_ f: @escaping (A, B, C, D) -> R, _ a: A, _ b: B) -> (C, D) -> R
{
    return { c, d in return f(a, b, c, d) }
}

func bindLeft<A, B, C, D, R>(_ f: @escaping (A, B, C, D) -> R, _ a: A) -> (B, C, D) -> R
{
    return { b, c, d in return f(a, b, c, d) }
}

func bindLeft<A, B, C, D, R>(_ f: @escaping (A, B, C, D) -> R, _ a: A, _ b: B, _ c: C) -> (D) -> R
{
    return { d in return f(a, b, c, d) }
}

func bindLeft<A, B, C, D, E, R>(_ f: @escaping (A, B, C, D, E) -> R, _ a: A) -> (B, C, D, E) -> R
{
    return { b, c, d, e in return f(a, b, c, d, e) }
}

func bindRight<A, B, C, D>(_ f: @escaping (A, B, C) -> D, _ c: C) -> (A, B) -> D
{
    return { a, b in return f(a, b, c) }
}

func bindRight<A, B, C, D>(_ f: @escaping (A, B, C) -> D, _ b: B, _ c: C) -> (A) -> D
{
    return { a in return f(a, b, c) }
}

func bindRight<A, B, C, D, E>(_ f: @escaping (A, B, C, D) -> E, _ b: B, _ c: C, _ d: D) -> (A) -> E
{
    return { a in return f(a, b, c, d) }
}

func bindRight<A, B, C, D, E, R>(_ f: @escaping (A, B, C, D, E) -> R, _ b: B, _ c: C, _ d: D, _ e: E) -> (A) -> R
{
    return { a in return f(a, b, c, d, e) }
}
