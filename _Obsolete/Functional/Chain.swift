//
//  Chain.swift
//  Sellel
//
//  Created by Ivan Goremykin on 26.12.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

func chain<A, B, R>(_ transform: @escaping (A) -> B, _ f: @escaping (B) -> R) -> (A) -> R
{
    return {
        a in
        
        return f(transform(a))
    }
}

func chain2<A, B, C, R>(_ transform: @escaping (A, B) -> C, _ f: @escaping (C) -> R) -> (A, B) -> R
{
    return {
        a, b in
        
        return f(transform(a, b))
    }
}

func chainLeft<A, B, C, R>(_ reverseTransform: @escaping (B) -> A, _ f: @escaping (A, C) -> R) -> (B, C) -> R
{
    return {
        b, c in
        
        return f(reverseTransform(b), c)
    }
}
