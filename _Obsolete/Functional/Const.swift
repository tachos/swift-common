//  Created by Ivan Goremykin on 21/05/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

func const<Item, Parameter>(_ value: Item) ->   (Parameter) -> Item
{
    return {
        _ in
        
        return value
    }
}
