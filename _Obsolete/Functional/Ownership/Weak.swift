//  Created by Ivan Goremykin on 15/03/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

struct Weak<Item> where Item: AnyObject
{
    public weak var value: Item?
    
    public init(_ value: Item?)
    {
        self.value = value
    }
}
