//
//  Unown.swift
//  Averia Collar
//
//  Created by Ivan Goremykin on 06/12/2018.
//  Copyright © 2018 Averia. All rights reserved.
//

// http://sveinhal.github.io/2016/03/16/retain-cycles-function-references/

import Foundation

// 0 args
func unown<A: AnyObject, B>(_ instance: A, _ classFunction: @escaping (A) -> ()->B) -> ()->B
{
    return {
        [unowned instance] in

        let instanceFunction = classFunction(instance)

        return instanceFunction()
    }
}

// 1 arg
func unown<T: AnyObject, U, V>(_ instance: T, _ classFunction: @escaping (T) -> (U)->V) -> (U)->V
{
    return {
        [unowned instance] args in
        
        let instanceFunction = classFunction(instance)
        
        return instanceFunction(args)
    }
}

// 2 ars
func unown<A: AnyObject, B, C, D>(_ instance: A, _ classFunction: @escaping (A) -> (B, C)->D) -> (B, C)->D
{
    return {
        [unowned instance] argB, argC in

        let instanceFunction = classFunction(instance)

        return instanceFunction(argB, argC)
    }
}

private func testUnown()
{
    class A
    {
        func foo(param: Int) { print(param) }
    }
    
    var a: A! = A()
    
    let unownedFoo = unown(a, A.foo)
    
    unownedFoo(42)
    
    a = nil
    
    unownedFoo(42) // Should crash here!
}
