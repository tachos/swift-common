//
//  Weak.swift
//  Averia Collar
//
//  Created by Ivan Goremykin on 10/12/2018.
//  Copyright © 2018 Averia. All rights reserved.
//

import Foundation

// MARK:- 0 arguments
func weaken<T: AnyObject>(_ instance: T, _ classFunction: @escaping (T) -> ()->Void) -> ()->Void
{
    return {
        [weak instance] in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            instanceFunction()
        }
    }
}

func weaken<T: AnyObject, R>(_ instance: T, _ classFunction: @escaping (T) -> ()-> R) -> ()->R
{
    return {
        [weak instance] in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction()
        }
        
        fatalError()
    }
}

func weaken<T: AnyObject, R>(_ instance: T, _ classFunction: @escaping (T) -> ()-> R, default: R) -> ()->R
{
    return {
        [weak instance] in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction()
        }
        
        return `default`
    }
}

// MARK:- 1 argument
func weaken<T: AnyObject, A>(_ instance: T, _ classFunction: @escaping (T) -> (A)->Void) -> (A)->Void
{
    return {
        [weak instance] args in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            instanceFunction(args)
        }
    }
}

func weaken<T: AnyObject, A, R>(_ instance: T, _ classFunction: @escaping (T) -> (A)->R, onNilReturn nilDefaultValue: R) -> (A)->R
{
    return {
        [weak instance] args in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction(args)
        }
        
        return nilDefaultValue
    }
}

// MARK:- 2 arguments
func weaken<T: AnyObject, A, B>(_ instance: T, _ classFunction: @escaping (T) -> (A, B)->Void) -> (A, B)->Void
{
    return {
        [weak instance] argA, argB in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            instanceFunction(argA, argB)
        }
    }
}

func weaken<T: AnyObject, A, R>(_ instance: T, _ classFunction: @escaping (T) -> (A)->R) -> (A)->R
{
    return {
        [weak instance] argA in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction(argA)
        }
        
        fatalError()
    }
}

func weaken<T: AnyObject, A, R>(_ instance: T, _ classFunction: @escaping (T) -> (A)->R, default: R) -> (A)->R
{
    return {
        [weak instance] argA in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction(argA)
        }
        
        return `default`
    }
}

func weaken<T: AnyObject, A, B, R>(_ instance: T, _ classFunction: @escaping (T) -> (A, B)->R) -> (A, B)->R
{
    return {
        [weak instance] argA, argB in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction(argA, argB)
        }
        
        fatalError()
    }
}

func weaken<T: AnyObject, A, B, C, R>(_ instance: T, _ classFunction: @escaping (T) -> (A, B, C)->R) -> (A, B, C)->R
{
    return {
        [weak instance] argA, argB, argC in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction(argA, argB, argC)
        }
        
        fatalError()
    }
}

// MARK:- 4 arguments
func weaken<T: AnyObject, A, B, C, D, R>(_ instance: T, _ classFunction: @escaping (T) -> (A, B, C, D)->R) -> (A, B, C, D)->R
{
    return {
        [weak instance] argA, argB, argC, argD in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction(argA, argB, argC, argD)
        }
        
        fatalError()
    }
}

private func testWeaken()
{
    class A
    {
        func foo(param: Int) { print(param) }
    }
    
    var a: A! = A()
    
    let weakenFoo = weaken(a, A.foo)
    
    weakenFoo(42)
    
    a = nil
    
    weakenFoo(42) // No crash, no print!
}
