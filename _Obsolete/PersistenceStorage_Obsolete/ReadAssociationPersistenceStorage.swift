//
//  ReadAssociationPersistenceStorage.swift
//  Sellel
//
//  Created by Ivan Goremykin on 24.01.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

class ReadAssociationPersistenceStorage<StoredType, AssociatedType>
{
    // MARK:- Typealias
    // MARK: Local Storage
    typealias GetAssociatedObjectsFromLocalStorageCallback = (_ object: StoredType) -> [AssociatedType]
    
    // MARK:- Local Storage
    fileprivate let getAssociatedObjectsFromLocalStorage: GetAssociatedObjectsFromLocalStorageCallback
    
    // MARK:- Initialization
    init(getAssociatedObjectsFromLocalStorage: @escaping GetAssociatedObjectsFromLocalStorageCallback)
    {
        self.getAssociatedObjectsFromLocalStorage = getAssociatedObjectsFromLocalStorage
    }
}

// MARK:- API
extension ReadAssociationPersistenceStorage
{
    func requestAssociatedObjectsFromLocalStorage(object: StoredType) -> [AssociatedType]
    {
        return getAssociatedObjectsFromLocalStorage(object)
    }
    
    func requestMultipleAssociatedObjectsFromLocalStorage(objects: [StoredType]) -> [[AssociatedType]]
    {
        return objects.map(requestAssociatedObjectsFromLocalStorage)
    }
}
