//
//  ReadWritePersistenceStorage.swift
//  Sellel
//
//  Created by Ivan Goremykin on 22.12.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//
import Alamofire

class ReadWritePersistenceStorage<IdentifierType, StoredType, CreationType, AssociatedType>: ReadPersistenceStorage<IdentifierType, StoredType>
{
    // MARK:- Typealias
    typealias ReadPersistenceStorageType = ReadPersistenceStorage<IdentifierType, StoredType>
    
    // MARK: Backend
    typealias CreateObjectAtBackendCallback = (_ creationObject: CreationType,
                                               _ responseCallback: @escaping (ResponseCode, IdentifierType?) -> Void) -> AsyncHandle
    typealias UpdateObjectAtBackendCallback = (_ updatedObject: StoredType,
                                               _ responseCallback: @escaping (ResponseCode) -> Void) -> AsyncHandle
    typealias RemoveObjectAtBackendCallback = (_ objectId: IdentifierType,
                                               _ responseCallback: @escaping (ResponseCode) -> Void) -> AsyncHandle
    
    // MARK: Local Storage
    typealias CreateObjectAtLocalStorageCallback            = (_ storedObject: StoredType) -> Void
    typealias UpdateObjectAtLocalStorageCallback            = (_ storedObject: StoredType) -> Void
    typealias RemoveObjectAtLocalStorageCallback            = (_ objectId: IdentifierType) -> Void
    typealias GetAssociatedObjectsAtLocalStorageCallback    = (_ objectId: IdentifierType) -> [AssociatedType]
    typealias UpdateAssociatedObjectsAtLocalStorageCallback = (_ objectId: IdentifierType, _ associatedObjects: [AssociatedType]) -> Void
    
    // MARK: Utils
    typealias ExtractIdCallback = (_ storedObject: StoredType) -> IdentifierType
    typealias CreateObjectCallback = (_ creationObject: CreationType, _ objectId: IdentifierType) -> StoredType
    typealias UpdateAssociatedObjectsCallback = (_ object: StoredType, _ associatedObjects: [AssociatedType]) -> StoredType
    
    // MARK:- Backend
    fileprivate let createObjectAtBackend: CreateObjectAtBackendCallback
    fileprivate let updateObjectAtBackend: UpdateObjectAtBackendCallback
    fileprivate let removeObjectAtBackend: RemoveObjectAtBackendCallback
    
    // MARK:- Local Storage
    fileprivate let createObjectAtLocalStorage:            CreateObjectAtLocalStorageCallback
    fileprivate let updateObjectAtLocalStorage:            UpdateObjectAtLocalStorageCallback
    fileprivate let removeObjectAtLocalStorage:            RemoveObjectAtLocalStorageCallback
    fileprivate let getAssociatedObjectsAtLocalStorage:    GetAssociatedObjectsAtLocalStorageCallback
    fileprivate let updateAssociatedObjectsAtLocalStorage: UpdateAssociatedObjectsAtLocalStorageCallback
    
    // MARK:- Utils
    fileprivate let extractId: ExtractIdCallback
    fileprivate let createObject: CreateObjectCallback
    fileprivate let updateAssociatedObjects: UpdateAssociatedObjectsCallback
    
    // MARK:- Initialization
    init(getObjectsFromBackend:      @escaping ReadPersistenceStorageType.GetObjectsFromBackendCallback,
         getObjectsFromLocalStorage: @escaping ReadPersistenceStorageType.GetObjectsFromLocalStorageCallback,
         saveObjectsToLocalStorage:  @escaping ReadPersistenceStorageType.SaveObjectsToLocalStorageCallback,
         createObjectAtBackend:      @escaping CreateObjectAtBackendCallback,
         createObjectAtLocalStorage: @escaping CreateObjectAtLocalStorageCallback,
         updateObjectAtBackend:      @escaping UpdateObjectAtBackendCallback,
         updateObjectAtLocalStorage: @escaping UpdateObjectAtLocalStorageCallback,
         removeObjectAtBackend:      @escaping RemoveObjectAtBackendCallback,
         removeObjectAtLocalStorage: @escaping RemoveObjectAtLocalStorageCallback,
         getAssociatedObjectsAtLocalStorage:    @escaping GetAssociatedObjectsAtLocalStorageCallback,
         updateAssociatedObjectsAtLocalStorage: @escaping UpdateAssociatedObjectsAtLocalStorageCallback,
         createObject:                          @escaping CreateObjectCallback,
         extractId:                             @escaping ExtractIdCallback,
         updateAssociatedObjects:               @escaping UpdateAssociatedObjectsCallback)
    {
        self.createObjectAtBackend      = createObjectAtBackend
        self.createObjectAtLocalStorage = createObjectAtLocalStorage
        
        self.updateObjectAtBackend      = updateObjectAtBackend
        self.updateObjectAtLocalStorage = updateObjectAtLocalStorage
        
        self.removeObjectAtBackend      = removeObjectAtBackend
        self.removeObjectAtLocalStorage = removeObjectAtLocalStorage
        
        self.getAssociatedObjectsAtLocalStorage    = getAssociatedObjectsAtLocalStorage
        self.updateAssociatedObjectsAtLocalStorage = updateAssociatedObjectsAtLocalStorage
        
        self.createObject = createObject
        self.extractId = extractId
        self.updateAssociatedObjects = updateAssociatedObjects
        
        super.init(getObjectsFromBackend:      getObjectsFromBackend,
                   getObjectsFromLocalStorage: getObjectsFromLocalStorage,
                   saveObjectsToLocalStorage:  saveObjectsToLocalStorage)
    }
}

// MARK:- API
extension ReadWritePersistenceStorage
{
    // Create an empty object.
    func create(creationObject: CreationType,
                onStart      startCallback:      @escaping VoidCallback,
                onSuccess    successCallback:    @escaping (IdentifierType) -> Void,
                onFailure    failureCallback:    @escaping (CreationType, ResponseCode) -> Void,
                onCompletion completionCallback: @escaping VoidCallback)
    {
        makeInOutRequest_Obsolete(request:            createObjectAtBackend,
                         requestObject:      creationObject,
                         onStart:           startCallback,
                         onCompletion:      completionCallback,
                         onFailure:         failureCallback,
                         onSuccess:         sequence2(
                                                chain2(createObject, createObjectAtLocalStorage),
                                                suppressLeft(successCallback)))
    }
    
    // Update an object itself
    func update(previousObject:            StoredType,
                newObject:                 StoredType,
                onApply applyCallback:     @escaping (StoredType) -> Void,
                onFailure failureCallback: @escaping (StoredType, ResponseCode) -> Void,
                onSuccess successCallback: @escaping VoidCallback)
    {
        makeInTransaction(requestObject: newObject,
                          request:       updateObjectAtBackend,
                          previousValue: previousObject,
                          newValue:      newObject,
                          onApplyValue:  sequence1(
                                            updateObjectAtLocalStorage,
                                            applyCallback),
                          onFailure:     failureCallback,
                          onSuccess:     suppress(successCallback))
    }
    
    // Update an object with new associated objects
    func updateAssociatedObjects(object: StoredType,
                                 newAssociatedObjects: [AssociatedType],
                                 onSuccess handleSuccess:               @escaping VoidCallback,
                                 onFailure handleFailure:               @escaping (StoredType, ResponseCode) -> Void)
    {
        let objectId = extractId(object)
        let previousAssociatedObjects = getAssociatedObjectsAtLocalStorage(objectId)
        let updatedObject = updateAssociatedObjects(object, newAssociatedObjects)
        
        makeInTransaction(requestObject: updatedObject,
                          request:       updateObjectAtBackend,
                          onApply:       bind(updateAssociatedObjectsAtLocalStorage, objectId, newAssociatedObjects),
                          onRevert:      bind(updateAssociatedObjectsAtLocalStorage, objectId, previousAssociatedObjects),
                          onFailure:     handleFailure,
                          onSuccess:     suppress(handleSuccess))
    }
    
    // Remove object
    func remove(storedObject:              StoredType,
                onFailure failureCallback: @escaping (StoredType, ResponseCode) -> Void,
                onSuccess successCallback: @escaping VoidCallback)
    {
        let objectId = extractId(storedObject)

        makeInTransaction(requestObject: objectId,
                          request:       removeObjectAtBackend,
                          onApply:       bind(removeObjectAtLocalStorage, objectId),
                          onRevert:      bind(createObjectAtLocalStorage, storedObject),
                          onFailure:     suppressLeft(bindLeft(failureCallback, storedObject)),
                          onSuccess:     suppress(successCallback))
    }
}
