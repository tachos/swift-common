//
//  Persistence.swift
//  Sellel
//
//  Created by Ivan Goremykin on 19.12.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//
import Alamofire

class ReadPersistenceStorage<IdentifierType, StoredType>
{
    // MARK:- Typealias
    // MARK: Backend
    typealias GetObjectsFromBackendCallback = (_ objectIds: [IdentifierType],
                                               _ responseCallback: @escaping (ResponseCode, [StoredType]?) -> Void) -> AsyncHandle
    
    // MARK: Local Storage
    typealias GetObjectsFromLocalStorageCallback = (_ objectIds: [IdentifierType]) -> [StoredType]
    typealias SaveObjectsToLocalStorageCallback  = (_ storedObjects: [StoredType]) -> Void
    
    // MARK:- Backend
    fileprivate let getObjectsFromBackend: GetObjectsFromBackendCallback
    
    // MARK:- Local Storage
    fileprivate let getObjectsFromLocalStorage: GetObjectsFromLocalStorageCallback
    fileprivate let saveObjectsToLocalStorage:  SaveObjectsToLocalStorageCallback
    
    // MARK:- Initialization
    init(getObjectsFromBackend:      @escaping GetObjectsFromBackendCallback,
         getObjectsFromLocalStorage: @escaping GetObjectsFromLocalStorageCallback,
         saveObjectsToLocalStorage:  @escaping SaveObjectsToLocalStorageCallback)
    {
        self.getObjectsFromBackend      = getObjectsFromBackend
        self.getObjectsFromLocalStorage = getObjectsFromLocalStorage
        self.saveObjectsToLocalStorage  = saveObjectsToLocalStorage
    }
}

// MARK:- API
extension ReadPersistenceStorage
{
    func requestObjects(dataSourceType:                        DataSourceType? = nil,
                        objectIds:                             [IdentifierType]? = nil,
                        onHandleObjects handleObjectsCallback: @escaping ([StoredType], DataSourceType) -> Void,
                        onFailure failureCallback:             @escaping ([IdentifierType], ResponseCode) -> Void)
    {
        let dummyObjectIds = objectIds ?? []
        
        let localHandler   = bind(handleObjectsCallback, getObjectsFromLocalStorage(dummyObjectIds), .local)
        let backendHandler = sequence1(
                                saveObjectsToLocalStorage,
                                bindRight(handleObjectsCallback, .backend))
        
        if let dataSourceType = dataSourceType
        {
            switch dataSourceType
            {
            case .local:
                localHandler()
                
            case .backend:
                makeInOutRequest_Obsolete(request:         getObjectsFromBackend,
                                 requestObject:   dummyObjectIds,
                                 onFailure: failureCallback,
                                 onSuccess: suppressLeft(backendHandler))
            }
        }
        else
        {
            makeInOutRequest_Obsolete(request:         getObjectsFromBackend,
                             requestObject:   dummyObjectIds,
                             onStart:   localHandler,
                             onFailure: failureCallback,
                             onSuccess: suppressLeft(backendHandler))
        }
    }
}

enum DataSourceType
{
    case local
    case backend
}
