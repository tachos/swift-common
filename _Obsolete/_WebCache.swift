//
//  UrlCache.swift
//  Sellel
//
//  Created by Ivan Goremykin on 28.11.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import RxSwift
import Alamofire

class WebCache<CachedType, RequestType, ResponseType> where RequestType: Hashable
{
    // MARK:- Typealias
    typealias RequestCallback = (RequestType, @escaping (ResponseCode, ResponseType?) -> Void) -> AsyncHandle
    typealias TransformCallback = (ResponseType) -> CachedType
    
    // MARK:- Cache
    fileprivate var cachedObjects: [RequestType : CachedType] = [:]
    fileprivate var pendingRequestObjects: Set<RequestType> = []
    
    // MARK:- ReactiveX
    fileprivate let cacheSubject = PublishSubject<(RequestType, ResponseType?)>()
    fileprivate let disposeBag = DisposeBag()
    
    // MARK:- Configuration
    fileprivate let request: RequestCallback
    fileprivate let transform: TransformCallback
    
    // MARK:- Initialization
    init(request: @escaping RequestCallback, transform: @escaping TransformCallback)
    {
        self.request = request
        self.transform = transform
        
        setupReactiveX()
    }
}

// MARK:- API
extension WebCache
{
    var cacheObservable: Observable<(RequestType, CachedType?)>
    {
        return cacheSubject
                    .asObservable()
                    .map { self.optionalTransform($0) }
                    .asObservable()
    }
    
    func getCachedObject(for requestObject: RequestType) -> CachedType?
    {
        return cachedObjects[requestObject]
    }
    
    func cacheMissingObjects(requestObjects:   [RequestType],
                             failureCallback:  @escaping (RequestType) -> Void,
                             successCallback:  @escaping ((RequestType, CachedType)) -> Void,
                             callerDisposeBag: DisposeBag)
    {
        subscribeOnNext(cacheObservable, createResponseCallback(failureCallback, successCallback), callerDisposeBag)
        
        requestObjects
            .filter { cachedObjects[$0] == nil }            // Skip already cached objects.
            .filter { !pendingRequestObjects.contains($0) } // Skip already requested objects.
            .forEach(cacheObjectFromBackend)                // Cache all missing objects.
    }
}

// MARK:- Backend
private extension WebCache
{
    func cacheObjectFromBackend(requestObject: RequestType)
    {
        pendingRequestObjects.update(with: requestObject)
        
        reuseInOutSubject(cacheSubject, request, requestObject)
    }
}

// MARK:- Setup
private extension WebCache
{
    func setupReactiveX()
    {
        subscribeOnNext(cacheObservable, onResponse, disposeBag)
    }
}

// MARK:- Events
private extension WebCache
{
    func onResponse(tuple: (RequestType, CachedType?))
    {
        if let url = tuple.1
        {
            print("SUCCESSSSSSS")
            
            cacheObject(requestObject: tuple.0, url: url)
        }
        else
        {
            print("ERRORRRRRRRR")
        }
        
        removePendingRequest(tuple.0)
    }
    
    private func cacheObject(requestObject: RequestType, url: CachedType)
    {
        cachedObjects[requestObject] = url
    }
    
    private func removePendingRequest(_ requestObject: RequestType)
    {
        pendingRequestObjects.remove(requestObject)
    }
}

// MARK:- Utils
private extension WebCache
{
    func optionalTransform(_ tuple: (RequestType, ResponseType?)) -> (RequestType, CachedType?)
    {
        return (tuple.0, tuple.1 == nil ? nil : transform(tuple.1!))
    }
    
    func createResponseCallback(_ failureCallback: @escaping (RequestType) -> Void,
                                _ successCallback: @escaping ((RequestType, CachedType)) -> Void) ->
        (RequestType, CachedType?) -> Void
    {
        return {
            requestObject, url in
            
            if let url = url
            {
                successCallback((requestObject, url))
            }
            else
            {
                failureCallback(requestObject)
            }
        }
    }
}
