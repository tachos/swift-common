//
//  Filter.swift
//  FoodPulse
//
//  Created by Ivan Goremykin on 14.02.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import Foundation

func skipNils<Type>(_ optionals: Optional<Type>...) -> [Type]
{
    return optionals.compactMap { $0 }
}
