//
//  AsyncFailure.swift
//  Broadway
//
//  Created by Ivan Goremykin on 18/02/2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

typealias AsyncResult<Item> = Result<Item, AsyncFailure>

public enum AsyncFailure
{
    case error(Error)
    case cancel
    case timeOut
}

extension AsyncFailure: Equatable
{
    public static func ==(lhs: AsyncFailure, rhs: AsyncFailure) -> Bool
    {
        switch (lhs, rhs)
        {
        case (.error, .error):
            return true
            
        case (.cancel, .cancel):
            return true
            
        case (.timeOut, .timeOut):
            return true
            
        default:
            return false
        }
    }
}

typealias AsyncFailureCallback = (AsyncFailure) -> Void
