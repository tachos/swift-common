//
//  ExecuteResult.swift
//  Broadway
//
//  Created by Ivan Goremykin on 18/02/2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

typealias ExecuteResult = Result<Void, ExecuteFailure>

enum ExecuteFailure
{
    case alreadyExecuting
    case conditionNotMet
}
