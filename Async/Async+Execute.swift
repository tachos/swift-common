//
//  ExecuteAsync.swift
//  Broadway
//
//  Created by Ivan Goremykin on 18/02/2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

// MARK:- AsyncInOut, AsyncIn
@discardableResult
func execute<Item, Parameter>(request:                       AsyncInOut<Item, Parameter>,
                              with parameter:                Parameter,
                              onStart handleStart:           VoidCallback? = nil,
                              onFailure handleFailure:       @escaping (AsyncFailure) -> Void,
                              onSuccess handleSuccess:       @escaping (Item) -> Void,
                              onCompletion handleCompletion: VoidCallback? = nil) -> AsyncHandle
{
    handleStart?()
    
    return request(parameter,
    {
        (result: Result<Item, AsyncFailure>) in
        
        switch result
        {
        case .success(let item):
            handleSuccess(item)
            
        case .failure(let reason):
            handleFailure(reason)
        }
        
        handleCompletion?()
    })
}

@discardableResult
func execute<Item, Parameter>(request: AsyncInOut<Item, Parameter>,
                              with parameter: Parameter,
                              onStart handleStart: VoidCallback? = nil,
                              onResult handleResult: @escaping AsyncResultHandler<Item>,
                              onCompletion handleCompletion: VoidCallback? = nil) -> AsyncHandle
{
    handleStart?()
    
    return request(parameter,
    {
        (result: Result<Item, AsyncFailure>) in
                    
        handleResult(result)
        
        handleCompletion?()
    })
}

// MARK:- AsyncOut
@discardableResult
func execute<Item>(request: AsyncOut<Item>,
                   onStart handleStart:           VoidCallback? = nil,
                   onFailure handleFailure:       @escaping (AsyncFailure) -> Void,
                   onSuccess handleSuccess:       @escaping (Item) -> Void,
                   onCompletion handleCompletion: VoidCallback? = nil) -> AsyncHandle
{
    handleStart?()
    
    return request(
    {
        (result: Result<Item, AsyncFailure>) in
        
        switch result
        {
        case .success(let item):
            handleSuccess(item)
            
        case .failure(let reason):
            handleFailure(reason)
        }
        
        handleCompletion?()
    })
}
