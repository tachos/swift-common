//
//  Cancelable.swift
//  Broadway
//
//  Created by Ivan Goremykin on 18/02/2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

protocol Cancelable
{
    @discardableResult
    func cancel() -> CancelResult
}

// MARK:- Failure
enum CancelFailure: Error
{
    case nothingToCancel
    case error(Error)
}

// MARK:- Result
typealias CancelResult = Result<Void, CancelFailure>
