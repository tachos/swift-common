//
//  Async2.swift
//  StatefulDataSource
//
//  Created by Ivan Goremykin on 18/03/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

// MARK:- Asyncs
typealias AsyncInOut<Item, Parameter> = (Parameter, @escaping AsyncResultHandler<Item>) -> AsyncHandle
typealias AsyncOut  <Item>            =            (@escaping AsyncResultHandler<Item>) -> AsyncHandle
typealias AsyncIn   <Parameter>       = (Parameter, @escaping AsyncResultHandler<Void>) -> AsyncHandle

// MARK:- Handle
protocol AsyncHandle: Cancelable
{
}

// MARK:- Handlers
typealias AsyncResultHandler<Item> = (AsyncResult<Item>) -> Void
typealias AsyncResultVoidHandler = AsyncResultHandler<Void>
