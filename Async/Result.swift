//
//  Result.swift
//  Broadway
//
//  Created by Ivan Goremykin on 18/02/2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

enum Result<Item, Failure>
{
    case success(Item)
    case failure(Failure)
    
    func mapFailure<MappedFailure>(_ transform: (Failure) -> MappedFailure) -> Result<Item, MappedFailure>
    {
        switch self
        {
        case .success(let item):
            return .success(item)
            
        case .failure(let failure):
            return .failure(transform(failure))
        }
    }
    
    var isSuccess: Bool
    {
        switch self
        {
        case .success:
            return true
            
        case .failure:
            return false
        }
    }
}
