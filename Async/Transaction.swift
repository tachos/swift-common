//
//  Transaction.swift
//  Broadway
//
//  Created by Ivan Goremykin on 19/06/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

struct Transaction
{
    static func performSymmetric<Parameter>(_ asyncIn: AsyncIn<Parameter>,
                                            newValue: Parameter,
                                            previousValue: Parameter,
                                            apply: @escaping (Parameter) -> Void,
                                            onFailure handleFailure: ((AsyncFailure) -> Void)?)
    {
        apply(newValue)
        
        _ = asyncIn(newValue,
        {
            switch $0
            {
            case .success:
                break
                
            case .failure(let asyncReason):
                apply(previousValue)
                handleFailure?(asyncReason)
            }
        })
    }
    
    static func performAsymmetric<Parameter>(_ asyncIn: AsyncIn<Parameter>,
                                             parameter: Parameter,
                                             onStart handleStart: @escaping VoidCallback,
                                             onFailure handleFailure: @escaping (AsyncFailure) -> Void,
                                             afterAll handleCompletion: @escaping VoidCallback)
    {
        handleStart()
        
        _ = asyncIn(parameter,
        {
            switch $0
            {
            case .success:
                break
                
            case .failure(let asyncReason):
                handleFailure(asyncReason)
            }
            
            handleCompletion()
        })
    }
}
