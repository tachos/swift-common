//
//  PaginatedAsync.swift
//  Broadway
//
//  Created by Ivan Goremykin on 18/02/2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

typealias PaginatedExecuteResult = Result<Void, PaginatedExecuteReason>

enum PaginatedExecuteReason
{
    case alreadyExecuting
    case conditionNotMet
    
    case noAvailablePage
}

typealias PaginatedAsync<Element, Parameter> = (Parameter, PaggeType, @escaping (PaginatedAsyncResult<Element>) -> Void) -> AsyncHandle
typealias PaginatedAsyncResult<Element> = Result<[Element], AsyncFailure>

func asPaginatedExecuteResult(_ executeResult: ExecuteResult) -> PaginatedExecuteResult
{
    return executeResult.mapFailure(asPaginatedExecuteReason)
}

func asPaginatedExecuteReason(_ executeReason: ExecuteFailure) -> PaginatedExecuteReason
{
    switch executeReason
    {
    case .alreadyExecuting:
        return .alreadyExecuting
        
    case .conditionNotMet:
        return .conditionNotMet
    }
}
