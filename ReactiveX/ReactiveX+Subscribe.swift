//
//  ReactiveX+Subscribe.swift
//  Sellel
//
//  Created by Ivan Goremykin on 13.11.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import RxSwift

func subscribeOnNext<ObservableType, ObservableParameterType>(_ observable: ObservableType,
                                                              _ callback: @escaping (ObservableParameterType) -> Void,
                                                              _ disposeBag: DisposeBag)
    where ObservableType: Observable<ObservableParameterType>
{
    observable.subscribe(onNext: callback).disposed(by: disposeBag)
}

func subscribeOnError<ObservableType, ObservableParameterType>(_ observable: ObservableType,
                                                               _ callback: @escaping (Error) -> Void,
                                                               _ disposeBag: DisposeBag)
    where ObservableType: Observable<ObservableParameterType>
{
    observable.subscribe(onError: callback).disposed(by: disposeBag)
}
