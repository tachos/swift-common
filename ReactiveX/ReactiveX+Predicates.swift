//  Created by Vyacheslav Shakaev on 16.04.2018.
//  Copyright © 2018 Tachos. All rights reserved.

import RxSwift

func ignoreNil<A>(x: A?) -> Observable<A>
{
    return x.map { Observable.just($0) } ?? Observable.empty()
}
