//  Created by Ivan Goremykin on 29/04/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import RxSwift

class Recorder<Item>
{
    private let disposeBag = DisposeBag()
    private (set) var recorded: [Item] = []
    
    init(_ observable: Observable<Item>)
    {
        observable
            .subscribe(onNext: weaken(self, Recorder.record))
            .disposed(by: disposeBag)
    }
    
    private func record(item: Item)
    {
        recorded.append(item)
    }
}
