//
//  ReactiveX+Utils.swift
//  Sellel
//
//  Created by Ivan Goremykin on 31.10.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import RxSwift

typealias VoidPublishSubject = PublishSubject<Void>
typealias VoidObservable = Observable<Void>

extension ObservableType
{
	func compactMap<R>(_ transform: @escaping (Element) throws -> R?) -> Observable<R>
	{
		return map(transform)
			.filter { $0 != nil }
			.map { $0! }
	}
}
