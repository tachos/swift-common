//
//  Tuples.swift
//  Sellel
//
//  Created by Ivan Goremykin on 28.12.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

func createTuples<Type1, Type2>(_ objects1: [Type1], _ objects2: [Type2]) -> [(Type1, Type2)]
{
    var tuples: [(Type1, Type2)] = []
    
    for index in 0..<objects1.count
    {
        tuples.append((objects1[index], objects2[index]))
    }
    
    return tuples
}
