//
//  ColonTime.swift
//  Sellel
//
//  Created by Ivan Goremykin on 16.11.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import AVFoundation

func getColonTime(_ time: CMTime) -> String
{
    let totalSeconds = CMTimeGetSeconds(time)
    
    let hours   = Int(totalSeconds / 3600)
    let minutes = Int(totalSeconds.truncatingRemainder(dividingBy: 3600) / 60)
    let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
    
    return hours > 0 ?
        String(format: "%i:%02i:%02i", hours, minutes, seconds) :
        String(format: "%02i:%02i", minutes, seconds)
}
