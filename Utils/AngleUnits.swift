//
//  AngleUnits.swift
//  Averia Collar
//
//  Created by Dmitry Cherednikov on 05/10/2018.
//  Copyright © 2018 Averia. All rights reserved.
//

import Foundation

func convertDegreesToRadians(_ degrees: Double) -> Double
{
    let angle = Measurement<UnitAngle>(value: degrees, unit: .degrees)

    return angle.converted(to: .radians).value
}
