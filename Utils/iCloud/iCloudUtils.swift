//  Created by Dmitry Cherednikov on 11/12/2017.
//  Copyright © 2017 Tachos. All rights reserved.

import Foundation

func excludeDirectoryFromICloudBackup(url: URL) throws
{
    var urlCopy = url

    var resourceValues = URLResourceValues()
    resourceValues.isExcludedFromBackup = true

    try urlCopy.setResourceValues(resourceValues)
}
