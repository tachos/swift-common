//  Created by Ivan Goremykin on 29/03/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

let MustImplementInDescendant = "Must implement in descendant"
