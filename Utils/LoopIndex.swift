//  Created by Ivan Goremykin on 17/04/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

func loopNextIndex(index: Int, count: Int) -> Int
{
    return (index + 1).remainderReportingOverflow(dividingBy: count).partialValue
}
