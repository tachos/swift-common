//
//  Keychain.swift
//  BBC
//
//  Created by Vyacheslav Shakaev on 17.05.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//
import SwiftKeychainWrapper

// MARK: - Static keys

/// Extend this class and add your user defaults keys as static constants
/// so you can use the shortcut dot notation (e.g. `Defaults[.yourKey]`)

public class KeychainKeys
{
    fileprivate init() {}
}

/// Base class for static user defaults keys. Specialize with value type
/// and pass key name to the initializer to create a key.

public class KeychainKey<ValueType>: KeychainKeys
{
    // TODO: Can we use protocols to ensure ValueType is a compatible type?
    public let _key: String
    
    public init(_ key: String)
    {
        self._key = key
        super.init()
    }
}

extension KeychainWrapper
{
    /// Returns `true` if `key` exists
    public func hasKey<T>(_ key: KeychainKey<T>) -> Bool
    {
        return object(forKey: key._key) != nil
    }
    
    /// Removes value for `key`
    public func remove<T>(_ key: KeychainKey<T>)
    {
        removeObject(forKey: key._key)
    }
}

fileprivate extension KeychainWrapper
{
    // String
    func getString(forKey key: String) -> String?
    {
        return string(forKey: key)
    }
    
    func setString(_ string: String, forKey key: String)
    {
        set(string, forKey: key)
    }
    
    // Number
    func number(for key: String) -> NSNumber?
    {
        return object(forKey: key) as? NSNumber
    }
    
    // Integer
    func getInt(forKey key: String) -> Int?
    {
        return integer(forKey: key)
    }
    
    func setInt(_ integer: Int, forKey key: String)
    {
        set(integer, forKey: key)
    }
    
    // [Integer]
    func getIntArray(forKey key: String) -> [Int]?
    {
        if let stringValue = getString(forKey: key)
        {
            return createIntArray(fromCommaSeparatedString: stringValue)
        }
        
        return nil
    }
    
    func setIntArray(_ intArray: [Int], forKey key: String)
    {
        setString(createCommaSeparatedString(intArray), forKey: key)
    }
    
    // Boolean
    func getBool(forKey key: String) -> Bool?
    {
        return bool(forKey: key)
    }
    
    func setBool(_ bool: Bool, forKey key: String)
    {
        set(bool, forKey: key)
    }
    
    // Float
    func getFloat(forKey key: String) -> Float?
    {
        return float(forKey: key)
    }
    
    func setFloat(_ float: Float, forKey key: String)
    {
        set(float, forKey: key)
    }
    
    // Double
    func getDouble(forKey key: String) -> Double?
    {
        return double(forKey: key)
    }
    
    func setDouble(_ value: Double, forKey key: String)
    {
        set(value, forKey: key)
    }
    
    // UInt64
    func getUInt64(forKey key: String) -> UInt64?
    {
        if let stringValue = getString(forKey: key)
        {
            return UInt64(stringValue, radix: 10)
        }
        
        return nil
    }
    
    func setUInt64(_ value: UInt64, forKey key: String)
    {
        setString(String(value), forKey: key)
    }
    
    // Date
    func getDate(forKey key: String) -> Date?
    {
        return object(forKey: key) as? Date
    }
    
    func setDate(_ date: Date, forKey key: String)
    {
        set(date as NSCoding, forKey: key)
    }
	
	// Strings dictionary
	func getStringsDictionary(forKey key: String) -> [String: String]?
	{
		return object(forKey: key) as? [String: String]
	}
	
	func setStringsDictionary(_ dictionary: [String: String], forKey key: String)
	{
		set(dictionary as NSCoding, forKey: key)
	}
    
    // Optional
    func setOptional<Type>(_ optional: Type?, forKey key: String, withSetter set: (Type, String) -> Void)
    {
        if let object = optional
        {
            set(object, key)
        }
        else
        {
            removeObject(forKey: key)
        }
    }
}

// MARK:- Subscripts for specific standard types

extension KeychainWrapper
{
    public subscript(key: KeychainKey<String?>) -> String?
    {
        get { return string(forKey: key._key) }
        set { setOptional(newValue, forKey: key._key, withSetter: setString) }
    }
    
    public subscript(key: KeychainKey<String>) -> String
    {
        get { return string(forKey: key._key)! }
        set { set(newValue, forKey: key._key) }
    }
    
    public subscript(key: KeychainKey<Int?>) -> Int?
    {
        get { return number(for: key._key)?.intValue }
        set { setOptional(newValue, forKey: key._key, withSetter: setInt) }
    }
    
    public subscript(key: KeychainKey<Int>) -> Int
    {
        get { return number(for: key._key)?.intValue ?? 0 }
        set { set(newValue, forKey: key._key) }
    }
    
    public subscript(key: KeychainKey<Double?>) -> Double?
    {
        get { return number(for: key._key)?.doubleValue }
        set { setOptional(newValue, forKey: key._key, withSetter: setDouble) }
    }
    
    public subscript(key: KeychainKey<Double>) -> Double
    {
        get { return number(for: key._key)?.doubleValue ?? 0.0 }
        set { set(newValue, forKey: key._key) }
    }
    
    public subscript(key: KeychainKey<Bool?>) -> Bool?
    {
        get { return number(for: key._key)?.boolValue }
        set { setOptional(newValue, forKey: key._key, withSetter: setBool) }
    }
    
    public subscript(key: KeychainKey<Bool>) -> Bool
    {
        get { return number(for: key._key)?.boolValue ?? false }
        set { set(newValue, forKey: key._key) }
    }
    
    public subscript(key: KeychainKey<Date?>) -> Date?
    {
        get { return object(forKey: key._key) as? Date }
        set { setOptional(newValue, forKey: key._key, withSetter: setDate) }
    }
	
	public subscript(key: KeychainKey<[String: String]?>) -> [String: String]?
	{
		get { return object(forKey: key._key) as? [String: String] }
		set { setOptional(newValue, forKey: key._key, withSetter: setStringsDictionary) }
	}
}

// MARK:- Generic enums

extension KeychainWrapper
{
    public subscript<E: RawRepresentable>(key: KeychainKey<E?>) -> E?  where E.RawValue == Int
    {
        get
        {
            if let rawValue = getInt(forKey: key._key)
            {
                return E(rawValue: rawValue)
            }
            return nil
        }
        set
        {
            setOptional(newValue?.rawValue, forKey: key._key, withSetter: setInt)
        }
    }
    
    public subscript<E: RawRepresentable>(key: KeychainKey<E>) -> E  where E.RawValue == Int
    {
        get { return E(rawValue: getInt(forKey: key._key)!)! }
        set { setInt(newValue.rawValue, forKey: key._key) }
    }
}

func createCommaSeparatedString(_ array: [Int]) -> String
{
    return array.map { String($0) }.joined(separator: ",")
}

func createIntArray(fromCommaSeparatedString commaSeparatedString: String) -> [Int]?
{
    let stringArray = commaSeparatedString.split(separator: ",")
    
    var intArray: [Int] = []
    intArray.reserveCapacity(stringArray.count)
    
    for string in stringArray
    {
        if let intValue = Int(string)
        {
            intArray.append(intValue)
        }
        else
        {
            return nil
        }
    }
    
    return intArray
}
