//
//  RawData.swift
//  Sellel
//
//  Created by Dmitry Cherednikov on 08/01/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import Foundation

func getObjectFromNSData<T>(_ data: NSData?, objectType: T.Type) -> T? where T: Decodable
{
    if let data = data
    {
        return NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? T
    }

    return nil
}

func getNSDataFromObject<ObjectType>(_ object: ObjectType) -> NSData where ObjectType: Encodable
{
    return NSKeyedArchiver.archivedData(withRootObject: object) as NSData
}
