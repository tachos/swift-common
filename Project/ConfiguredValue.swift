//
//  ConfiguredValue.swift
//  Broadway
//
//  Created by Ivan Goremykin on 14/02/2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

func configuredValue<T>(debug: T, release: T) -> T
{
#if DEBUG
    return debug
#else
    return release
#endif
}
