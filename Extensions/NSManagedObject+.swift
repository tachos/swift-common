//
//  NSManagedObject+.swift
//  Sellel
//
//  Created by Dmitry Cherednikov on 08/01/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import CoreData

extension NSManagedObject
{
    static var entityName: String
    {
        return String(describing: self)
    }
}
