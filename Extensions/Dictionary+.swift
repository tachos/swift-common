//
//  Dictionary+.swift
//  StarHub
//
//  Created by Ivan Goremykin on 25.05.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

func + <Key,Value>(left: Dictionary<Key,Value>, right: Dictionary<Key,Value>) -> Dictionary<Key,Value>
{
    var map = Dictionary<Key,Value>()
    map.reserveCapacity(left.count + right.count)

    map = left
    for (k, v) in right { map[k] = v }

    return map
}

// stores the value in the dictionary only if it's not null
//https://stackoverflow.com/a/32413471
infix operator =?

func =? <T> ( lhs: inout T, rhs: T?)
{
    if let value = rhs
    {
        lhs = value
    }
}



//
//  Created by Vitaly Trofimov on 09.03.2021.
//  Copyright © 2021 Hice. All rights reserved.
//
extension Dictionary
{
	@inlinable func mapKeys<K: Hashable>(_ transform: (Key) throws -> K) rethrows -> [K: Value]
	{
		return Dictionary<K, Value>(
			uniqueKeysWithValues: try map { (key, value) in
				return (try transform(key), value)
			}
		)
	}
	
	@inlinable func compactMapKeys<K: Hashable>(_ transform: (Key) throws -> K?) rethrows -> [K: Value]
	{
		return Dictionary<K, Value>(
			uniqueKeysWithValues: try compactMap { (key, value) in
				return try transform(key)
					.map { ($0, value) }
			}
		)
	}
}


extension Dictionary
{
	func mapKeyAndValues_Recursive<K: Hashable, V>(
		transformKey: (Key) -> K,
		transformValue: (Value) -> V
	) -> [K: V]
	{
		return Dictionary<K, V>(
			uniqueKeysWithValues: map { (key, value) in
				
				var mappedDictionaryValue: V?

				if let valueDictionary = value as? Dictionary
				{
					mappedDictionaryValue = valueDictionary.mapKeyAndValues_Recursive(
						transformKey: transformKey,
						transformValue: transformValue
					) as? V
				}
				
				return (
					transformKey(key),
					mappedDictionaryValue ?? transformValue(value)
				)
			}
		)
	}
}
