//
//  RandomAccessCollection+.swift
//
//  Created by Vitaly Trofimov on 28.07.2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation



/// provides read-only access to a '2D-collection' [[]] by an IndexPath
extension RandomAccessCollection
	where Index == Int,
		Element: RandomAccessCollection,
		Element.Index == Int
{
	subscript(indexPath: IndexPath) -> Element.Element
	{
		return self[indexPath.section][indexPath.row]
	}
}
