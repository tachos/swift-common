//
//  Comparable+.swift
//  Currentus
//
//  Created by Dmitry Cherednikov on 11/02/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

extension Comparable
{
    func clamped(to range: ClosedRange<Self>) -> Self
    {
        let min = range.lowerBound, max = range.upperBound

        return self < min ? min : (max < self ? max : self)
    }
}
