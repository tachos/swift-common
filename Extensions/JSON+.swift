//
//  JSON+.swift
//  Hice
//
//  Created by Dmitry Cherednikov on 19.01.2021.
//  Copyright © 2021 Hice. All rights reserved.
//

import Foundation
import SwiftyJSON



extension JSON
{
	var nonEmptyString: String?
	{
		if let string = string,
		   !string.isEmpty
		{
			return string
		}

		return nil
	}
	
	func decimal(fractionDigits: Int?) -> Decimal?
	{
		if let value = double
		{
			var rawDecimalValue = Decimal(value)
			if let fractionDigits = fractionDigits
			{
				var decimalValue = Decimal()
				NSDecimalRound(
					&decimalValue,
					&rawDecimalValue,
					fractionDigits,
					.plain
				)
				
				return decimalValue
			}
			else
			{
				return rawDecimalValue
			}
		}
		
		return nil
	}
}
