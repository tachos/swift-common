//
//  Int+.swift
//  Sellel
//
//  Created by Dmitry Cherednikov on 01/12/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

extension Int
{
    // NOTE: The function handles just the most basic grammar case
    // when making the entity name plural.
    func toString(entityName: String) -> String
    {
        let isSingular = (self == 1)
        let appendingString = isSingular ? entityName : entityName + "s"

        return String(self) + " " + appendingString
    }
}
