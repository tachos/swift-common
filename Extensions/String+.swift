//
//  String+.swift
//  StarHub
//
//  Created by Ivan Goremykin on 02.06.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

func sanitizeWhitespaces(string: String) -> String
{
    return string.trimmingCharacters(in: .whitespacesAndNewlines)
}

func sanitizeUrlString(_ urlString: String) -> String
{
    return urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
}

extension String
{
    func substrings(matchingRegex regex: String) -> [String]
    {
        do
        {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            return results.map { nsString.substring(with: $0.range) }
        }
        catch let error
        {
            print("invalid regex: \(error.localizedDescription)")
            
            return []
        }
    }
    
    func containsSpecialCharacters() -> Bool
    {
        var illegalCharacterSet = NSCharacterSet.alphanumerics.inverted
        
        illegalCharacterSet.remove(" ")
        
        if let _ = self.rangeOfCharacter(from: illegalCharacterSet)
        {
            return true
        }
        else
        {
            return false
        }
    }
}

extension String
{
    var commaSeparatedValues: [String]
    {
        if self.isEmpty
        {
            return []
        }
        else
        {
            return self.components(separatedBy: ",")
        }
    }
    
    var presentationForUrl: String
    {
        return self.replacingOccurrences(of: " ", with: "+").addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""
    }
}

extension String
{
    var firstUppercased: String
    {
        guard let first = first else { return "" }
        
        return String(first).uppercased() + dropFirst()
    }
}

extension String
{
    func uppercasedFirstLetter() -> String
    {
        return isEmpty ? "" : prefix(1).uppercased() + dropFirst()
    }
    
    func lowercasedFirstLetter() -> String
    {
        return isEmpty ? "" : prefix(1).lowercased() + dropFirst()
    }
}

// https://successfulcoder.com/2016/12/17/how-to-calculate-height-of-a-multiline-string-in-swift/
extension String
{
    func calculateHeight(constrainedWidth: CGFloat, font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: constrainedWidth,
                                    height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: [.usesLineFragmentOrigin, .usesFontLeading],
                                            attributes: [NSAttributedString.Key.font: font],
                                            context: nil)
        return boundingBox.height
    }
}

// https://stackoverflow.com/a/49072718
extension String
{
    var isValidUrl: Bool
    {
        let detector = try! NSDataDetector(
            types: NSTextCheckingResult.CheckingType.link.rawValue
        )

        if let match = detector.firstMatch(
            in: self,
            options: [],
            range: NSRange(location: 0, length: self.utf16.count))
        {
            // it is a link, if the match covers the whole string
            return match.range.length == self.utf16.count
        }

        return false
    }
}

extension String
{
	func trimmingTrailingWhitespaces() -> String
	{
		return trimmingCharacters(in: .whitespaces)
	}
}
