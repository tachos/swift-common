//
//  Date+.swift
//  Sellel
//
//  Created by Dmitrii Morozov on 28/10/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import Foundation

// MARK:- String
extension Date
{
    func toString(dateFormat: String) -> String
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        return dateFormatter.string(from: self)
    }
    
    func toString() -> String
    {
        let dateFormatter: DateFormatter = DateFormatter()
        
        dateFormatter.locale    = Locale(identifier: Localization.currentLanguageCode)
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        
        return dateFormatter.string(from: self)
    }
}

// MARK:- Static Methods
extension Date
{
    static var yesterday: Date
    {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date())!
    }
    
    static var yearStart: Date
    {
        let year = Calendar.current.component(.year, from: Date())
        
        var components = DateComponents()
        components.day = 1
        components.month = 1
        components.year = year
        
        return Calendar.current.date(from: components)!
    }

    static var monthStart: Date
    {
        var components = Date().components
        components.day = 1

        return Date.date(from: components)!
    }

    static var currentYear: Int
    {
        return Date().year
    }
}

// MARK:- Instance Methods
extension Date
{
    var month: Int
    {
        return components.month ?? 0
    }

    var year: Int
    {
        return components.year ?? 0
    }

    var timeless: Date
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day, .timeZone], from: self)
        let timelessDate = calendar.date(from: components)!

        return timelessDate
    }

    func updated(month: Int) -> Date
    {
        var components = self.components
        components.month = month

        return Date.date(from: components)!
    }

    func updated(year: Int) -> Date
    {
        var components = self.components
        components.year = year

        return Date.date(from: components)!
    }

    func date(yearsAgo: Int) -> Date
    {
        var components = self.components
        components.year = components.year! - yearsAgo

        return Date.date(from: components)!
    }

    func date(daysAgo: Int) -> Date
    {
        let timeInterval = TimeInterval(daysAgo * 24 * 60 * 60)

        return self.addingTimeInterval(-timeInterval)
    }
}

// MARK:- Utils
private extension Date
{
    var components: DateComponents
    {
        return Calendar.current.dateComponents([.minute, .hour, .day, .month, .year], from: self)
    }

    static func date(from components: DateComponents) -> Date?
    {
        var calendar      = Calendar.current
        calendar.timeZone = TimeZone.current
        calendar.locale   = Locale.current

        return calendar.date(from: components)
    }
}

func createDate(_ year: Int, _ month: Int, _ day: Int) -> Date
{
    let calendar = Calendar(identifier: .gregorian)
    let components = DateComponents(year: year, month: month, day: day)
    return calendar.date(from: components)!
}
