//
//  NSAttributedString+.swift
//  Broadway
//
//  Created by Vyacheslav Shakaev on 18/09/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

extension NSAttributedString
{
    func calculateHeight(containerWidth: CGFloat) -> CGFloat
    {
        let rect = self.boundingRect(with: CGSize(width: containerWidth, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        return ceil(rect.size.height)
    }
    
    func calculateWidth(containerHeight: CGFloat) -> CGFloat
    {
        let rect = self.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: containerHeight), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        return ceil(rect.size.width)
    }
}

/// Created by Vitaly Trofimov on 18.07.2018.
extension NSAttributedString
{
    func replacingOccurences(of placeholder: String, with value: String) -> NSAttributedString
    {
        let attributedText = NSMutableAttributedString(attributedString: self)
        let text = NSMutableString(string: attributedText.string)
        
        var searchLocaltion = 0
        var searchRange = NSMakeRange(0, text.length)
        var range = text.range(of: placeholder, range: searchRange)
        while range.location != NSNotFound
        {
            let attributes = attributedText.attributes(at: range.location, effectiveRange: nil)
            let attributedValue = NSAttributedString(string: value, attributes: attributes)
            attributedText.replaceCharacters(in: range, with: attributedValue)
            text.replaceCharacters(in: range, with: value)
            
            searchLocaltion = range.location + value.count
            searchRange = NSMakeRange(searchLocaltion, text.length - searchLocaltion)
            
            range = text.range(of: placeholder, range: searchRange)
        }
        
        return attributedText
    }
    
}
