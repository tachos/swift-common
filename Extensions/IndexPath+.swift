//  Created by Ivan Goremykin on 30.05.2018.
//  Copyright © 2018 Tachos. All rights reserved.

import UIKit

func getNextRowIndexPath(_ indexPath: IndexPath) -> IndexPath
{
    return IndexPath(row: indexPath.row + 1, section: indexPath.section)
}

func getPreviuosRowIndexPath(_ indexPath: IndexPath) -> IndexPath
{
    return IndexPath(row: indexPath.row - 1, section: indexPath.section)
}
