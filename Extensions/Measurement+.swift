//  Created by Dmitry Cherednikov on 24/08/2018.
//  Copyright © 2018 Tachos. All rights reserved.

import Foundation

extension Measurement
{
    static func +=( lhs: inout Measurement, rhs: Measurement)
    {
        lhs = lhs + rhs
    }
}
