//
//  NSMutableAttributedString+.swift
//  FoodPulse
//
//  Created by Dmitry Cherednikov on 15/03/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import Foundation

extension NSMutableAttributedString
{
    public func setAsLink(textToFind: String, linkUrl: String, options: NSString.CompareOptions = [])
    {
        let foundRange = self.mutableString.range(of: textToFind, options: options) 

        if foundRange.location != NSNotFound
        {
            addAttribute(NSAttributedString.Key.link, value: linkUrl, range: foundRange)
        }
    }
    
    public func addAttributes(textToFind: String,
                              attributes: [NSAttributedString.Key: Any],
                              options: NSString.CompareOptions = [])
    {
        let foundRange = self.mutableString.range(of: textToFind, options: options)
        
        if foundRange.location != NSNotFound
        {
            addAttributes(attributes, range: foundRange)
        }
    }
}
