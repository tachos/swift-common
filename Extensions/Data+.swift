//
//  DataUtils.swift
//  Hice
//
//  Created by Vitaly Trofimov on 17.12.2020.
//  Copyright © 2020 Hice. All rights reserved.
//

import Foundation




extension Data
{
	func toHexString() -> String
	{
		return map { String(format: "%02hhX", $0) }
			.joined()
	}
}
