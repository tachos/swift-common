//
//  Array+.swift
//  nextv
//
//  Created by Ivan Goremykin on 24.02.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import Foundation

extension Array
{
    static func excludeNils(_ array: [Element?]) -> [Element]
    {
        return array
                .filter { $0 != nil }
                .map { $0! }
    }
}

extension Array
{
    func splitToPairs() -> [(Element, Element)]
    {
        if self.count <= 1 { return [] }
        
        var arr: [(Element, Element)] = []
        
        for i in 1..<self.count
        {
            arr.append((self[i-1],self[i]))
        }
        
        return arr
    }
}

extension Array
{
    var tail: Array
    {
        return Array(self.dropFirst())
    }
}

extension Array
{
    func shuffledCopy() -> Array
    {
        var selfCopy = self
        selfCopy.shuffle()

        return selfCopy
    }

    mutating func shuffle()
    {
        for _ in 0..<count
        {
            sort { (_,_) in arc4random() < arc4random() }
        }
    }
}

extension Array where Element == String
{
    func toCommaSeparated() -> String
    {
        return self.joined(separator: ",")
    }
}

func ==<T: Equatable>(lhs: [T]?, rhs: [T]?) -> Bool
{
    switch (lhs, rhs)
    {
    case (.some(let lhs), .some(let rhs)):
        return lhs == rhs
        
    case (.none, .none):
        return true
        
    default:
        return false
    }
}

extension Array
{
    @discardableResult
    mutating func removeFirst(where predicate: (Element) -> Bool) -> Bool
    {
        if let itemIndex = self.firstIndex(where: predicate)
        {
            self.remove(at: itemIndex)
            return true
        }
        return false
    }
}

extension Array
{
    var lastIndex: Int?
    {
        if count == 0
        {
            return nil
        }

        return count - 1
    }

    mutating func makeElementFirstMaintainingOrder(initialPosition: Int)
    {
        let firstPart = self.suffix(from: initialPosition)
        let secondPart = self.prefix(upTo: initialPosition)

        self = Array(firstPart) + Array(secondPart)
    }

    var loopedInterleavingPairs: [(Element, Element)]
    {
        if count < 2
        {
            return []
        }

        var pairs: [(Element, Element)] = []
        let rangeEnd = count - 2

        for index in 0...rangeEnd
        {
            pairs.append((self[index], self[index + 1]))
        }

        pairs.append((self[count - 1], self[0]))

        return pairs
    }

    func getPreviousLoopedElement(for index: Int) -> Element
    {
        return self[getPreviousLoopedIndex(for: index)]
    }

    func getNextLoopedElement(for index: Int) -> Element
    {
        return self[getNextLoopedIndex(for: index)]
    }

    func getPreviousLoopedIndex(for index: Int) -> Int
    {
        emitErrorIfIndexOutOfRange(index)

        if count == 0 || count == 1
        {
            return 0
        }

        return (index - 1) >= 0 ? (index - 1) : (count - 1)
    }

    func getNextLoopedIndex(for index: Int) -> Int
    {
        emitErrorIfIndexOutOfRange(index)

        if count == 0 || count == 1
        {
            return 0
        }

        return (index + 1) < count ? (index + 1) : 0
    }

    private func emitErrorIfIndexOutOfRange(_ index: Int)
    {
        if !(0..<count).contains(index)
        {
            fatalError("index out of range")
        }
    }
}

extension Array
{
    public func isValidIndex(_ index: Int) -> Bool
    {
        return index >= 0 && index < self.endIndex
    }
}

extension Array
{
    public subscript(safeIndex index: Int) -> Element?
    {
        if index >= 0, index < self.endIndex
        {
            return self[index]
        }
        
        return nil
    }
}

extension Array
{
    func chunked(into size: Int) -> [[Element]]
    {
        return stride(from: 0, to: count, by: size).map
        {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}

extension Array
{
    @discardableResult
    mutating func remove(at indices: [Int]) -> [Element]
    {
        var deletedItems: [Element] = []
        let sortedIndices = Array<Int>(Set(indices))
            .sorted(by: >) // filter duplicated indices, sort from largest to smallest

        sortedIndices.forEach
        {
            deletedItems.append(self.remove(at: $0))
        }

        return deletedItems
    }
}

extension Array
{
	func first(_ numberOfElements: Int) -> Array<Element>
	{
		if numberOfElements > count
		{
			return self
		}

		return enumerated().compactMap
		{
			(index, element) in

			if index < numberOfElements
			{
				return element
			}

			return nil
		}
	}

	func last(_ numberOfElements: Int) -> Array<Element>
	{
		if numberOfElements > count
		{
			return self
		}

		let smallestIndex = count - numberOfElements

		return enumerated().compactMap
		{
			(index, element) in

			if index >= smallestIndex
			{
				return element
			}

			return nil
		}
	}
	
	/// returns nil if the index is out-of-bounds
	subscript(safe index: Int) -> Element?
	{
		guard index < count
		else
		{
			return nil
		}
		
		return self[index]
	}
}


/// allows to index an array of arrays [[]] with an IndexPath both for reading and writing
extension Array	// we cannot overload [] for MutableCollection as it would conflict with RandomAccessCollection
	where Element: MutableCollection,
		  Element.Index == Int
{
	subscript(indexPath: IndexPath) -> Element.Element
	{
		get
		{
			return self[indexPath.section][indexPath.row]
		}
		set
		{
			self[indexPath.section][indexPath.row] = newValue
		}
	}
}


extension Array
	where Element: MutableCollection & RangeReplaceableCollection,
		  Element.Index == Int
{
	@discardableResult
	mutating func remove(at indexPath: IndexPath) -> Element.Element
	{
		return self[indexPath.section].remove(at: indexPath.row)
	}
}
