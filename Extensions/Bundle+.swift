//
//  Bundle+.swift
//  Sellel
//
//  Created by Ivan Goremykin on 05.09.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import Foundation

extension Bundle
{
    var releaseVersionNumber: String
    {
        return (infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
    }
    
    var buildVersionNumber: String
    {
        return (infoDictionary?["CFBundleVersion"] as? String) ?? ""
    }
}
