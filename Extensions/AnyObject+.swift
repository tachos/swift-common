//
//  AnyObject+.swift
//  Vegcard
//
//  Created by Vitaly Trofimov on 13.11.2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

func bundle(forClassOf object: AnyObject) -> Bundle
{
	return Bundle(for: type(of: object).self)
}
