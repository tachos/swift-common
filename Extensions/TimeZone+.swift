//
//  TimeZone+.swift
//  Broadway
//
//  Created by Ivan Goremykin on 04/02/2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

extension TimeZone
{
    /// E.g. "+08:00"
    /// https://stackoverflow.com/questions/42235257/how-to-get-timezone-offset-as-%C2%B1hhmm
    var gmtOffset: String
    {
        let seconds = secondsFromGMT()

        let hours = seconds / 3600
        let minutes = abs(seconds/60) % 60
        
        return String(format: "%+.2d:%.2d", hours, minutes)
    }
}
