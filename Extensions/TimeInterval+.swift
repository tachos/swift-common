//
//  TimeInterval+.swift
//  Broadway
//
//  Created by Vyacheslav Shakaev on 13/02/2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

extension TimeInterval
{
    init(minutes: TimeInterval)
    {
        self = minutes * TimeInterval(60)
    }
}
