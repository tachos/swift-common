//
//  Bool+.swift
//  Broadway
//
//  Created by Vyacheslav Shakaev on 19/08/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

public func |= (lhs: inout Bool, rhs: Bool)
{
    lhs = lhs || rhs
}

public func &= (lhs: inout Bool, rhs: Bool)
{
    lhs = lhs && rhs
}

public func ^= (lhs: inout Bool, rhs: Bool)
{
    lhs = lhs != rhs
}
