//  Created by Ivan Goremykin on 10/12/2018.
//  Copyright © 2018 Ivan Goremykin. All rights reserved.

// http://sveinhal.github.io/2016/03/16/retain-cycles-function-references/

import Foundation

// MARK:- 0 Arguments
func w<T: AnyObject>(_ instance: T, _ classFunction: @escaping (T) -> ()->Void)
                            -> ()->Void
{
    return {
        [weak instance] in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            instanceFunction()
        }
    }
}

func w<T: AnyObject, R>(_ instance: T, _ classFunction: @escaping (T) -> ()->R, onNilReturn nilDefault: R)
                            -> ()->R
{
    return {
        [weak instance] in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction()
        }
        
        return nilDefault
    }
}

// MARK:- 1 Argument
func w<T: AnyObject, A>(_ instance: T, _ classFunction: @escaping (T) -> (A)->Void)
                            -> (A) -> Void
{
    return {
        [weak instance] args in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            instanceFunction(args)
        }
    }
}

func w<T: AnyObject, A, R>(_ instance: T, _ classFunction: @escaping (T) -> (A) -> R, onNilReturn nilDefault: R)
                            -> (A) -> R
{
    return {
        [weak instance] args in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction(args)
        }
        
        return nilDefault
    }
}

// MARK:- 2 Arguments
func w<T: AnyObject, A, B>(_ instance: T, _ classFunction: @escaping (T) -> (A, B)->Void)
                            -> (A, B)->Void
{
    return {
        [weak instance] argA, argB in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            instanceFunction(argA, argB)
        }
    }
}

func w<T: AnyObject, A, B, R>(_ instance: T, _ classFunction: @escaping (T) -> (A,B)->R, onNilReturn nilDefault: R)
                            -> (A,B)->R
{
    return {
        [weak instance] argA, argB in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction(argA, argB)
        }
        
        return nilDefault
    }
}

// MARK:- 3 Arguments
func w<T: AnyObject, A, B, C>(_ instance: T, _ classFunction: @escaping (T) -> (A, B, C)->Void)
                            -> (A, B, C)->Void
{
    return {
        [weak instance] argA, argB, argC in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            instanceFunction(argA, argB, argC)
        }
    }
}

func w<T: AnyObject, A, B, C, R>(_ instance: T, _ classFunction: @escaping (T) -> (A,B,C)->R, onNilReturn nilDefault: R)
                            -> (A,B,C)->R
{
    return {
        [weak instance] argA, argB, argC in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction(argA, argB, argC)
        }
        
        return nilDefault
    }
}

// MARK:- 4 Arguments
func w<T: AnyObject, A, B, C, D>(_ instance: T, _ classFunction: @escaping (T) -> (A, B, C, D)->Void)
                            -> (A, B, C, D)->Void
{
    return {
        [weak instance] argA, argB, argC, argD in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            instanceFunction(argA, argB, argC, argD)
        }
    }
}

func w<T: AnyObject, A, B, C, D, R>(_ instance: T, _ classFunction: @escaping (T) -> (A, B, C, D)->R, onNilReturn nilDefault: R)
                            -> (A, B, C, D)->R
{
    return {
        [weak instance] argA, argB, argC, argD in
        
        if let instance = instance
        {
            let instanceFunction = classFunction(instance)
            
            return instanceFunction(argA, argB, argC, argD)
        }
        
        return nilDefault
    }
}
