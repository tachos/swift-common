//  Created by Ivan Goremykin on 26.12.2017.
//  Copyright © 2017 Ivan Goremykin. All rights reserved.

func c<A, B, R>(_ f0: @escaping (A) -> B, _ f1: @escaping (B) -> R) -> (A) -> R
{
    return {
        a in
        
        return f1(f0(a))
    }
}

func c<A, B, R>(_ f1: @escaping (A,A)->R, _ f0: @escaping (B)->A) -> (B,B)->R
{
    return {
        b1, b2 in
        
        return f1(f0(b1), f0(b2))
    }
}
