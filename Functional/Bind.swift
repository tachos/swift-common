//  Created by Ivan Goremykin on 05.09.17.

// MARK:- All
func b<A, R>(_ f: @escaping (A) -> R, _ a: A) -> () -> R
{
    return { return f(a) }
}

func b<A, B, R>(_ f: @escaping (A, B) -> R, _ a: A, _ b: B) -> () -> R
{
    return { return f(a, b) }
}

func b<A, B, C, R>(_ f: @escaping (A, B, C) -> R, _ a: A, _ b: B, _ c: C) -> () -> R
{
    return { return f(a, b, c) }
}

func b<A, B, C, D, R>(_ f: @escaping (A, B, C, D) -> R, _ a: A, _ b: B, _ c: C, _ d: D) -> () -> R
{
    return { return f(a, b, c, d) }
}

// MARK:- Left

// MARK: 1 Argument
func bl<A, B, R>(_ f: @escaping (A,B)->R, _ a: A) -> (B)->R
{
    return { b in return f(a, b) }
}

func bl<A, B, C, R>(_ f: @escaping (A, B, C) -> R, _ a: A) -> (B, C) -> R
{
    return { b, c in return f(a, b, c) }
}

func bl<A, B, C, D, R>(_ f: @escaping (A, B, C, D) -> R, _ a: A) -> (B, C, D) -> R
{
    return { b, c, d in return f(a, b, c, d) }
}

func bl<A, B, C, D, E, R>(_ f: @escaping (A, B, C, D, E) -> R, _ a: A) -> (B, C, D, E) -> R
{
    return { b, c, d, e in return f(a, b, c, d, e) }
}

// MARK: 2 Arguments
func bl<A, B, C, R>(_ f: @escaping (A, B, C) -> R, _ a: A, _ b: B) -> (C) -> R
{
    return { c in return f(a, b, c) }
}

func bl<A, B, C, D, R>(_ f: @escaping (A, B, C, D) -> R, _ a: A, _ b: B) -> (C, D) -> R
{
    return { c, d in return f(a, b, c, d) }
}

// MARK: 3 Arguments
func bl<A, B, C, D, R>(_ f: @escaping (A, B, C, D) -> R, _ a: A, _ b: B, _ c: C) -> (D) -> R
{
    return { d in return f(a, b, c, d) }
}

// MARK:- Right

// MARK: 1 Argument
func br<A, B, R>(_ f: @escaping (A, B) -> R, _ b: B) -> (A) -> R
{
    return { a in return f(a, b) }
}

func br<A, B, C, R>(_ f: @escaping (A, B, C) -> R, _ c: C) -> (A, B) -> R
{
    return { a, b in return f(a, b, c) }
}

// MARK: 2 Arguments
func br<A, B, C, R>(_ f: @escaping (A, B, C) -> R, _ b: B, _ c: C) -> (A) -> R
{
    return { a in return f(a, b, c) }
}

// MARK: 3 Arguments
func br<A, B, C, D, R>(_ f: @escaping (A, B, C, D) -> R, _ b: B, _ c: C, _ d: D) -> (A) -> R
{
    return { a in return f(a, b, c, d) }
}

// MARK: 4 Arguments
func br<A, B, C, D, E, R>(_ f: @escaping (A, B, C, D, E) -> R, _ b: B, _ c: C, _ d: D, _ e: E) -> (A) -> R
{
    return { a in return f(a, b, c, d, e) }
}
