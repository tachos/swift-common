//  Created by Ivan Goremykin on 21/05/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

func const<Item, A>(_ value: Item) -> (A)->Item
{
    return {
        _ in
        
        return value
    }
}
