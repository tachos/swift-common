//  Created by Ivan Goremykin on 31.08.17.
//  Copyright © 2017 Tachos. All rights reserved.

func sequence(_ callbacks: VoidCallback?...) -> VoidCallback
{
    return { callbacks.forEach{ $0?() } }
}

func sequence<A>(_ callbacks: ((A) -> Void)?...) ->
    (A) -> Void
{
    return {
        a in
        
        callbacks.forEach{ $0?(a) }
    }
}

func sequence<A, B>(_ callbacks: ((A, B) -> Void)?...) ->
    (A, B) -> Void
{
    return {
        a, b in
        
        callbacks.forEach{ $0?(a, b) }
    }
}
