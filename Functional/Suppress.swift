//  Created by Ivan Goremykin on 06.09.17.
//  Copyright © 2017 Ivan Goremykin. All rights reserved.

// MARK:- All
func s<A, R>(_ f: @escaping () -> R) -> (A) -> R
{
    return { _ in return f() }
}

func s<A, B, R>(_ f: @escaping () -> R) -> (A,B)->R
{
    return { _, _ in return f() }
}

// MARK:- Left
func sl<A, B, R>(_ f: @escaping (B) -> R) -> (A,B)->R
{
    return { _, b in return f(b) }
}

func sl<A, B, C, R>(_ f: @escaping (B, C) -> R) -> (A,B,C)->R
{
    return { _, b, c in return f(b, c) }
}

// MARK:- Right
func sr<A, B, R>(_ f: @escaping (A) -> R) -> (A,B)->R
{
    return { a, _ in f(a) }
}

func sr<A, B, C, R>(_ f: @escaping (A) -> R) -> (A,B,C)->R
{
    return { a, _, _ in f(a) }
}
