//
//  Result+Apply.swift
//  Common
//
//  Created by Ivan Goremykin on 17/08/2020.
//  Copyright © 2020 Ivan Goremykin. All rights reserved.
//

import Foundation

public extension Result {
    func apply(
        failure applyFailure: (Failure) -> Void,
        success applySuccess: (Success) -> Void
    ) {
        switch self {
        case .failure(let error):
            applyFailure(error)
        case .success(let success):
            applySuccess(success)
        }
    }

    func apply(
        failure applyFailure: (Failure) -> Void
    ) {
        switch self {
        case .failure(let error):
            applyFailure(error)
        case .success:
            break
        }
    }

    func apply(
        success applySuccess: (Success) -> Void
    ) {
        switch self {
        case .failure:
            break
        case .success(let success):
            applySuccess(success)
        }
    }
}
