//
//  Optional+Apply.swift
//  Common
//
//  Created by Ivan Goremykin on 17/08/2020.
//  Copyright © 2020 Ivan Goremykin. All rights reserved.
//

import Foundation

public extension Optional {
    func apply(
        none applyNone: () -> Void,
        some applySome: (Wrapped) -> Void
    ) {
        switch self {
        case .none:
            applyNone()
        case .some(let wrapped):
            applySome(wrapped)
        }
    }

    func apply(some applySome: (Wrapped) -> Void) {
        if let some = self {
            applySome(some)
        }
    }

    func apply(none applyNone: () -> Void) {
        if self == nil {
            applyNone()
        }
    }
}
