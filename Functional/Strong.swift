//
//  Strong.swift
//  Broadway
//
//  Created by Ivan Goremykin on 06/02/2020.
//  Copyright © 2020 Tachos. All rights reserved.
//

import Foundation

// MARK:- 0 Arguments
func strong<T: AnyObject>(_ instance: T, _ classFunction: @escaping (T) -> ()->Void)
                            -> ()->Void
{
    return {
        let instanceFunction = classFunction(instance)
        
        instanceFunction()
    }
}

// MARK:- 1 Argument
func strong<T: AnyObject, A>(_ instance: T, _ classFunction: @escaping (T) -> (A)->Void)
                            -> (A) -> Void
{
    return {
        args in
        
        let instanceFunction = classFunction(instance)
        
        instanceFunction(args)
    }
}
