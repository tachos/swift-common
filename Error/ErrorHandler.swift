//  Created by Ivan Goremykin on 16.08.2018.
//  Copyright © 2018 Tachos. All rights reserved.

import Foundation

typealias ErrorHandler = (Error) -> Void
