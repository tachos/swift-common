//
//  ErrorText.swift
//  FoodPulse
//
//  Created by Ivan Goremykin on 04.02.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//
import Foundation

struct ErrorText
{
    let errorTitle: String
    let errorDescription: String
    let error: NSError?
    
    init(errorTitle: String, errorDescription: String)
    {
        self.errorTitle = errorTitle
        self.errorDescription = errorDescription
        self.error = nil
    }
    
    init(errorTitle: String, errorDescription: String, error: NSError)
    {
        self.errorTitle = errorTitle
        self.errorDescription = errorDescription
        self.error = error
    }
}

typealias ErrorTextCallback = (ErrorText) -> Void
