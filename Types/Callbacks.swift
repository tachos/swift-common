//  Created by Ivan Goremykin on 12/07/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

typealias VoidCallback   = ()       -> Void
typealias StringCallback = (String) -> Void
typealias IntCallback    = (Int)    -> Void
typealias BoolCallback   = (Bool)   -> Void
typealias UrlCallback    = (URL) -> Void
