//
//  MeasurementAliases.swift
//
//  Created by Dmitry Cherednikov on 24/08/2018.
//  Copyright © 2018 Averia. All rights reserved.
//

import Foundation

typealias Length   = Measurement<UnitLength>
typealias Area     = Measurement<UnitArea>
typealias Duration = Measurement<UnitDuration>
typealias Mass     = Measurement<UnitMass>
typealias Energy   = Measurement<UnitEnergy>

extension Measurement where UnitType == UnitLength
{
    static var zero: Measurement<UnitLength>
    {
        return Length(value: Double(0), unit: .meters)
    }
}

extension Measurement where UnitType == UnitArea
{
    static var zero: Measurement<UnitArea>
    {
        return Area(value: Double(0), unit: .squareMeters)
    }
}

extension Measurement where UnitType == UnitDuration
{
    static var zero: Measurement<UnitDuration>
    {
        return Duration(value: Double(0), unit: .seconds)
    }
}

extension Measurement where UnitType == UnitMass
{
    static var zero: Measurement<UnitMass>
    {
        return Measurement(value: Double(0), unit: .kilograms)
    }
}

extension Measurement where UnitType == UnitEnergy
{
    static var zero: Measurement<UnitEnergy>
    {
        return Measurement(value: Double(0), unit: .calories)
    }
}
