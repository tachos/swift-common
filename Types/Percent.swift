//
//  Percent.swift
//  Sellel
//
//  Created by Dmitry Cherednikov on 16/11/2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import UIKit

struct Percent: Comparable
{
    fileprivate let value: Double

    private init()
    {
        value = Double()
    }
}

// MARK:- Comparable
extension Percent
{
    static func <(lhs: Percent, rhs: Percent) -> Bool
    {
        return lhs.value < rhs.value
    }

    static func ==(lhs: Percent, rhs: Percent) -> Bool
    {
        return lhs.value == lhs.value
    }
}

// MARK:- Addition and Substraction
extension Percent
{
    static func +(lhs: Percent, rhs: Percent) -> Percent
    {
        return Percent(percent: lhs.value + rhs.value)
    }

    static func -(lhs: Percent, rhs: Percent) -> Percent
    {
        return Percent(percent: lhs.value - rhs.value)
    }

    static func +=(lhs: inout Percent, rhs: Percent)
    {
        lhs = Percent(percent: lhs.value + rhs.value)
    }

    static func -=(lhs: inout Percent, rhs: Percent)
    {
        lhs = Percent(percent: lhs.value - rhs.value)
    }
}

// MARK:- Multiplication
extension Percent
{
    static func *(doubleValue: Double, percentValue: Percent) -> Double
    {
        return doubleValue * percentValue.normalized
    }

    static func *(percentValue: Percent, doubleValue: Double) -> Double
    {
        return doubleValue * percentValue.normalized
    }

    static func *(floatValue: Float, percentValue: Percent) -> Float
    {
        return floatValue * Float(percentValue.normalized)
    }

    static func *(percentValue: Percent, floatValue: Float) -> Float
    {
        return floatValue * Float(percentValue.normalized)
    }

    static func *(floatValue: CGFloat, percentValue: Percent) -> CGFloat
    {
        return floatValue * CGFloat(percentValue.normalized)
    }

    static func *(percentValue: Percent, floatValue: CGFloat) -> CGFloat
    {
        return floatValue * CGFloat(percentValue.normalized)
    }
    
    static func *(lhs: Percent, rhs: Percent) -> Percent
    {
        return Percent(percent: lhs.normalized * rhs.value)
    }
}

// MARK:- Initializers
extension Percent
{
    init(percent floatValue: Float)
    {
        value = Double(floatValue)
    }

    init(percent doubleValue: Double)
    {
        value = doubleValue
    }

    init(percent intValue: Int)
    {
        value = Double(intValue)
    }
    
    init(normalized: Double)
    {
        value = normalized * Double(100)
    }
    
    init(normalized: Float)
    {
        value = Double(normalized) * Double(100)
    }

    static var zero: Percent
    {
        return Percent(normalized: 0.0)
    }
}

// MARK:- Getters
extension Percent
{
    public var double: Double
    {
        return value
    }

    public var float: Float
    {
        return Float(value)
    }

    public var int: Int
    {
        return Int(value)
    }

    public var normalized: Double
    {
        return value / 100
    }
}

// MARK:- UI Slider
func createPercent(fromSliderValue sliderValue: Float) -> Percent
{
    return Percent(normalized: sliderValue)
}

extension Percent
{
    var uiSliderValue: Float
    {
        return Float(normalized)
    }
}
