//
//  Msisdn.swift
//  Sellel
//
//  Created by Ivan Goremykin on 30.08.17.
//  Copyright © 2017 Tachos. All rights reserved.
//
import Foundation

struct Msisdn
{
    let countryCode:      String    // _excluding_ the leading '+' sign
    let subscriberNumber: String

    init?(countryCode: String, subscriberNumber: String)
    {
        guard let countryCodeNumber = UInt64(countryCode, radix: 10) else
        {
            return nil
        }

        let countryCodeWithoutPlusSign = String(countryCodeNumber)
        self.countryCode        = countryCodeWithoutPlusSign
        self.subscriberNumber   = subscriberNumber
    }
}

extension Msisdn
{
    var fullNumber: String
    {
        return countryCode + subscriberNumber
    }
    
    var isEmpty: Bool
    {
        return countryCode.isEmpty || subscriberNumber.isEmpty
    }
}

// MARK:- Equatable
extension Msisdn: Equatable
{
    static func ==(lhs: Msisdn, rhs: Msisdn) -> Bool
    {
        return lhs.countryCode == rhs.countryCode &&
               lhs.subscriberNumber == rhs.subscriberNumber
    }
}

struct MsisdnFormatter
{
    func createMsisdn(from string: String) -> Msisdn?
    {
        let regex = "(\\+\\d{1,3}|0\\d{1,3}|00\\d{1,2})([-\\/\\s.]|\\d)+"
        
        let parts = matches(for: regex, in: string)
        
        if parts.count != 2
        {
            return nil
        }

        return Msisdn(countryCode: parts[0], subscriberNumber: parts[1])
    }
}

// http://jayeshkawli.ghost.io/regular-expressions-in-swift-ios/
func matches(for regex: String, in text: String) -> [String]
{
    do {
        let regex = try NSRegularExpression(pattern: regex)
        let results = regex.matches(in: text,
                                    range: NSRange(text.startIndex..., in: text))
        let finalResult = results.map {
            String(text[Range($0.range, in: text)!])
        }
        return finalResult
    } catch let error {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}
