//
//  FunctionTypes.swift
//  FoodPulse
//
//  Created by Dmitry Cherednikov on 20/12/2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

typealias Handler<Item> = (Item) -> Void
typealias Handler2<Item1, Item2> = (Item1, Item2) -> Void
typealias Getter<Item>  = () -> Item

class OneShotFunc
{
    private var function: VoidCallback?

    init(_ function: @escaping VoidCallback)
    {
        self.function = function
    }

    init() {}

    func performIfFirstTime()
    {
        if let function = function
        {
            function()

            self.function = nil
        }
    }
}
