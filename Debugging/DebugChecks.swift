//
//  DebugChecks.swift
//  Hice
//
//  Created by Vyacheslav Shakaev on 14.01.2021.
//  Copyright © 2021 Tachos. All rights reserved.
//

import Foundation


// MARK: - Debug Assertions (you might want to put breakpoints into them)

// NOTE: compile with CRASH_ON_ASSERTION_FAILURE enabled to enable crashes in debug builds.
// More specifically, go to
// "Add Project / Build Settings / Swift Compiler - Custom Flags / Active Compilation Conditions"
// and add CRASH_ON_ASSERTION_FAILURE to "Debug" compilation conditions:
// https://stackoverflow.com/a/56625657
//
// NOTE: make sure to enable the DEBUG flag, otherwise assertion failures won't crash!


func dbgcheck(
	_ condition: Bool
	, _ message: String = ""
	, file: StaticString = #file, function: StaticString = #function, line: Int = #line
)
{
	
	#if DEBUG
	
	if !condition
	{
		debugPrint("🆘 [DBG] Assertion failed in \(function), \(file):\(line),\n\(message)")
		// put a breakpoint here!
		
		#if CRASH_ON_ASSERTION_FAILURE
		assert(condition, message)
		#endif
	}
	
	#endif
	
}

func dbgcheck(
	_ condition: Bool, _ message: String
	, file: String = #file, function: String = #function, line: Int = #line
)
{
	#if DEBUG
	if !condition
	{
		print("🆘Assertion failed in \(function), \(file):\(line),\n\(message)")
		
		#if CRASH_ON_ASSERTION_FAILURE
		assert(condition, message)
		#endif
	}
	#endif
}


func dbgUnreachable(
	_ message: String = ""
	, function: StaticString = #function, file: StaticString = #file, line: Int = #line
)
{
	#if DEBUG
	debugPrint("🆘 [DBG] Unreachable code executed in \(function), \(file):\(line),\n\(message)")
	// put a breakpoint here!
	
	#if CRASH_ON_ASSERTION_FAILURE
	assert(false, message)
	#endif
	
	#endif
}
