//  Created by Tachos Test on 19.11.2018.
//  Copyright © 2018 Tachos. All rights reserved.

import UIKit


#if DEBUG

public func TODO(_ message: String = "", file: String = #file, function: String = #function, line: Int = #line)
{
    let text = "😡TODO: \(message) in \(function), \(file):\(line)"
    debugPrint(text)
}

#else

public func TODO(_ message: String = "")
{}

#endif


//func dbgcheckMainThread(
//    file: String = #file, function: String = #function, line: Int = #line
//)
//{
//    dbgcheck(Thread.isMainThread, file: file, function: function, line: line)
//}

// https://nshipster.com/filemanager/
func dbg_listBundleContents(_ bundle: Bundle)
{
    #if DEBUG

    do {
        let items = try FileManager.default.contentsOfDirectory(
            atPath: bundle.resourcePath!
        )
        print("👀Found \(items.count) folders in bundle \(bundle):")
        for pair in items.enumerated() {
            print("➡️[\(pair.offset)]: \(pair.element)")
        }
    } catch {
        dbgwarn("Failed to read bundle contents: \(error.localizedDescription)")
    }

    #endif
}





// https://medium.com/@dmytro.anokhin/overview-of-developer-tools-for-ui-debugging-122e4995f972

extension UIView
{
    var recursiveDescription: NSString
    {
        let selector: Selector = NSSelectorFromString("recursiveDescription")
        let result = perform(selector)
        return result?.takeUnretainedValue() as! NSString
    }
}

extension UIViewController
{
    var printHierarchy: NSString
    {
        let selector: Selector = NSSelectorFromString("_printHierarchy")
        let result = perform(selector)
        return result?.takeUnretainedValue() as! NSString
    }
}

extension UIViewController
{
    func findChildViewController<ViewController: UIViewController>(type: ViewController.Type) -> ViewController?
    {
        if let vc = self as? ViewController
        {
            return vc
        }
        
        for viewController in self.children
        {
            if let child = viewController.findChildViewController(type: type)
            {
                return child
            }
        }
        
        return nil
    }
}

extension UIViewController
{
    func findParentViewController<ViewController: UIViewController>(type: ViewController.Type) -> ViewController?
    {
        if let vc = self as? ViewController
        {
            return vc
        }
        
        if let parentViewController = self.parent
        {
            return parentViewController.findParentViewController(type: type)
        }

        return nil
    }
}

extension UIViewController
{
    func findPresentedViewController<ViewController: UIViewController>(type: ViewController.Type) -> ViewController?
    {
        if let vc = self as? ViewController
        {
            return vc
        }
        
        if let presentedViewController = self.presentedViewController
        {
            return presentedViewController.findPresentedViewController(type: type)
        }

        return nil
    }
}

extension UIViewController
{
    func findPresentingViewController<ViewController: UIViewController>(type: ViewController.Type) -> ViewController?
    {
        if let vc = self as? ViewController
        {
            return vc
        }
        
        if let parentViewController = self.presentingViewController
        {
            return parentViewController.findPresentingViewController(type: type)
        }
        
        return nil
    }
}

extension UITableViewCell {
    var parentTableView: UITableView? {
        return self.parentView(of: UITableView.self)
    }
}

/// for debugging
struct InitializationCounter
{
    public private(set) var _count = 0
    private let _name: String!
    
    init(_ name: String)
    {
        _count = 0
        _name = name
    }
    
    mutating func inc()
    {
        if _count > 0
        {
            dbgout("\(_name!) has already been called \(_count) times!")
        }
        
        _count += 1
    }
    
    mutating func dec()
    {
        assert(_count > 0)
        _count -= 1
    }
    
    var isInitialized: Bool
    {
        return _count > 0
    }
    
    mutating func reset()
    {
        _count = 0
    }
}

class DebugUtil
{
    static func generateUniqueIdBasedOnCurrentDateTime() -> String
    {
        let now = Date()
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: now)
        let month = calendar.component(.month, from: now)
        let hour = calendar.component(.hour, from: now)
        let minutes = calendar.component(.minute, from: now)
        let seconds = calendar.component(.second, from: now)
        
        return "\(year)z\(month)z\(hour)z\(minutes)z\(seconds)"
    }
}
