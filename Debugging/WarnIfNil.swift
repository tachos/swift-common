//
//  WarnIfNil.swift
//  RSchip
//
//  Created by Vyacheslav Shakaev on 12.01.2021.
//  Copyright © 2021 Tachos. All rights reserved.
//

import Foundation



extension Optional
{
	/// useful for catching errors when parsing, e.g.:
	///
	/// json["counter_party_cs"].string.warnIfNil(),
	/// or
	/// let status = json["status"].string
	///		.flatMap({ AccountStatus(rawValue: $0).warnIfNil() }),
	///
	@inline(__always)
	func warnIfNil(
		function: StaticString = #function,
		file: StaticString = #file,
		line: Int = #line
	) -> Self
	{
		
		#if DEBUG
		
		if self == nil
		{
			// put a breakpoint here!
			debugPrint("🆘 [DBG] Unexpected NULL in \(function) at \(line), \(file)")
		}
		
		#endif
		
		return self
	}
}
