//  Created by Ivan Goremykin on 02/04/2019.
//  Copyright © 2019 Tachos. All rights reserved.

import Foundation

struct DebugFlag
{
    static func falseInRelease(_ flag: Bool) -> Bool
    {
        #if DEBUG
        return flag
        #else
        return false
        #endif
    }
}
