//
//  Debugging+Touches.swift
//  BBC
//
//  Created by Vyacheslav Shakaev on 05.06.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import IQKeyboardManagerSwift
import UIKit

// https://stackoverflow.com/questions/26109023/how-to-debug-who-is-eating-my-touches-in-uikit

class TestCollectionViewCell: UICollectionViewCell
{
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView?
    {
        let hitView = super.hitTest(point, with: event)
        if let hitView = hitView
        {
            print("DBG😡Hit view \(hitView) of type \(type(of: hitView)), VC: \(String(describing: hitView.parentViewController))")
        }
        return hitView
    }
}

extension UIResponder
{
    // Go up in responder hierarchy until we reach a ViewController
    // or return nil if we don't find one
    var parentViewController: UIViewController?
    {
        if let vc = self as? UIViewController {
            return vc
        }
        
        return next?.parentViewController
    }
}

class TestWindow: UIWindow
{
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView?
    {
        let hitView = super.hitTest(point, with: event)
//        dbgout("Hit \(hitView?.viewContainingController()) at \(point)")
        return hitView
    }
}
