//
//  Debugging+Sounds.swift
//  Sellel
//
//  Created by Vyacheslav Shakaev on 22.04.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

import AVFoundation

class Debugging
{
    //
}

// MARK:- Sounds
extension Debugging
{
    // Taken from: https://nikaeblog.wordpress.com/2017/07/11/system-sound-id-list-ios/

    static func playTweetSound()
    {
        AudioServicesPlaySystemSound(1016)
    }

    static func playFanfare()
    {
        AudioServicesPlaySystemSound(1025)
    }
    
    static func playLowPower()
    {
        AudioServicesPlaySystemSound(1006)
    }
    
    static func playSound(_ id: UInt32)
    {
        AudioServicesPlaySystemSound(id)
    }
}
