//
//  DebugLogging.swift
//  Hice
//
//  Created by Vyacheslav Shakaev on 14.01.2021.
//  Copyright © 2021 Tachos. All rights reserved.
//

import Foundation


// MARK: - Debug Logging


func dbgout(_ message: String)
{
	#if DEBUG
	debugPrint("✅\(message)")
	#endif
}


func dbgwarn(_ message: String)
{
	#if DEBUG
	debugPrint("😡\(message)")
	#endif
}


/// can be handy to print out function calls during debugging
public func dbgtrace(
	_ message: String = ""
	, function: StaticString = #function, file: StaticString = #file, line: Int = #line
)
{
	
	#if DEBUG
	
	let text = "➡️ [DBG] \(message) in \(function), \(file):\(line)"
	debugPrint(text)
	
	#endif
	
}
