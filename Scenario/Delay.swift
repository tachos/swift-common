//
//  Delay.swift
//  Testing
//
//  Created by Ivan Goremykin on 25/04/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

func execute(delay: TimeInterval, _ callback: @escaping VoidCallback)
{
    DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: callback)
}
