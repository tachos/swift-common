//
//  Tree.swift
//  Testing
//
//  Created by Vyacheslav Shakaev on 22/03/2019.
//  Copyright © 2019 Tachos. All rights reserved.
//

import Foundation

///
class Tree<Item>
{
    let item: Item
    
    private(set) weak var parent: Tree?
    private(set) var brother: Tree?
    private(set) var first_kid: Tree?
    
    init(item: Item)
    {
        self.item = item
    }
    
    func addChild(_ node: Tree<Item>)
    {
        dbgcheck(node.isOrphan)
        
        node.brother = first_kid
        first_kid = node
        
        node.parent = self
    }
    
    func countChildren() -> Int
    {
        var childCount = 0
        
        var curr = first_kid
        
        while curr != nil
        {
            let next = curr!.brother
            
            childCount += 1
            
            curr = next
        }
        
        return childCount
    }
    
    var isOrphan: Bool
    {
        return nil == parent
    }
    
    func removeAllChildren()
    {
        var curr = first_kid
        while curr != nil
        {
            curr!.parent = nil
            curr = curr!.brother
        }
        
        first_kid = nil
    }
    
    // MARK:- Map
    func map<MappedItem>(_ transform: (Item) -> (MappedItem)) -> Tree<MappedItem>
    {
        let newTreeNode = Tree<MappedItem>(item: transform(item))
        //
        var curr = first_kid
        
        while curr != nil
        {
            let next = curr!.brother
            
            newTreeNode.addChild(curr!.map(transform))
            
            curr = next
        }
        
        return newTreeNode
    }
    
    // MARK:- Debugging
    var dbgname: String
    {
        return "\(address(self))"
    }
}

private
func address<T: AnyObject>(_ o: T) -> Int
{
    return unsafeBitCast(o, to: Int.self)
}
