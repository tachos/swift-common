//
//  NilCheck.swift
//  nextv
//
//  Created by Ivan Goremykin on 19.01.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

import Foundation

func areNotNil(_ optionals: Optional<Any> ...) -> Bool
{
    return !(optionals.contains { $0 == nil })
}

func anyIsNil(_ optionals: Optional<Any> ...) -> Bool
{
    return optionals.contains { $0 == nil }
}

func isNilOrEmpty(_ string: String?) -> Bool
{
    return string == nil || string!.isEmpty
}

func isNotNil(_ optional: Optional<Any>) -> Bool
{
    return optional != nil
}

func anyStringIsNilOrEmpty(_ strings: String?...) -> Bool
{
    return any(c({ $0 }, isNilOrEmpty), strings)
}

/// Type safe method to check if collection is nil and if not if it has values
func isNilOrEmpty<CollectionType>(collection: CollectionType?) -> Bool where CollectionType: Collection
{
    guard let collection = collection else { return true }
    return collection.isEmpty
}

/// Extension on Options with constraint to collection type
extension Optional where Wrapped: Collection
{
    var isNilOrEmpty: Bool
    {
        get
        {
            guard let value = self else { return true }
            return value.isEmpty
        }
    }

    var hasElements: Bool
    {
        return !isNilOrEmpty
    }
}
