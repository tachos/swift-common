//
//  StringOrEmpty.swift
//  Sellel
//
//  Created by Ivan Goremykin on 05.09.17.
//  Copyright © 2017 Tachos. All rights reserved.
//

func stringOrEmpty(_ string: String?) -> String
{
    return string ?? ""
}
