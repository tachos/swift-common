//
//  Any.swift
//  FoodPulse
//
//  Created by Ivan Goremykin on 07.02.2018.
//  Copyright © 2018 Tachos. All rights reserved.
//

func any<Type>(_ isTrue: (Type) -> Bool, _ objects: [Type]) -> Bool
{
    for object in objects
    {
        if isTrue(object)
        {
            return true
        }
    }
    
    return false
}
