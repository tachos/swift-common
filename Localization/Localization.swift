//
//  Localization.swift
//  Sellel
//
//  Created by Ivan Goremykin on 28.12.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import Localize_Swift
import RxCocoa
import RxSwift

class Localization
{
    static var currentLanguageCode: String
    {
        return Localize_Swift.Localize.currentLanguage()
    }
    
    static let stringsTableName = "Strings"
    static let alertsTableName = "Alerts"
}

func localizeAlert(_ string: String) -> String
{
    return string.localized(using: Localization.alertsTableName)
}

func localizeDialog(_ string: String) -> String
{
    return string.localized(using: "Dialogs")
}

func localizeEnum(_ string: String) -> String
{
    return string.localized(using: "Enums")
}

func localizeString(_ string: String) -> String
{
    return string.localized(using: Localization.stringsTableName)
}

func localizeValidator(_ string: String) -> String
{
    return string.localized(using: "Validators")
}

func localizeActionSheet(_ string: String) -> String
{
    return string.localized(using: "ActionSheet")
}

func localizeToast(_ string: String) -> String
{
    return string.localized(using: "Toasts")
}

func localizeError(_ string: String) -> String
{
    return string.localized(using: "Errors")
}

func setupLocalization(_ localizeUI: @escaping VoidCallback, _ disposeBag: DisposeBag)
{
    localizeUI()
    subscribeToLanguageChange(localizeUI, disposeBag)
}

func subscribeToLanguageChange(_ handleLanguageChanged: @escaping VoidCallback, _ disposeBag: DisposeBag)
{
    NotificationCenter.default.rx
        .notification(Notification.Name(LCLLanguageChangeNotification))
        .subscribe(onNext: s(handleLanguageChanged))
        .disposed(by: disposeBag)
}
