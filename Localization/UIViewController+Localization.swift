//
//  +Localization.swift
//  Sellel
//
//  Created by Ivan Goremykin on 28.12.2017.
//  Copyright © 2017 Tachos. All rights reserved.
//

import Localize_Swift

// MARK:- Language Switch Notification
extension UIViewController
{
    func startObservingLanguageSwitchNotification(_ selector: Selector)
    {
        performSelector(onMainThread: selector, with: nil, waitUntilDone: true)
        
        NotificationCenter.default.addObserver(self, selector: selector, name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    func stopObservingLanguageSwitchNotification()
    {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK:- Localized String
extension UIViewController
{
    func localize(_ string: String) -> String
    {
        return string.localized(using: viewControllerTable)
    }
    
    private func localize(_ string: String, from table: String) -> String
    {
        return string.localized(using: table)
    }
    
    private var viewControllerTable: String
    {
        return String(describing: type(of: self))
    }
}

// MARK:- UILabel
extension UIViewController
{
    func localize(_ label: UILabel, _ string: String)
    {
        label.text = self.localize(string)
    }
}

// MARK:- UIButton
extension UIViewController
{
    func localize(_ button: UIButton, _ string: String)
    {
        button.setTitle(self.localize(string), for: .normal)
    }
}
